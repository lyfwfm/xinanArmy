﻿#!/bin/bash
svn up

echo $LOGNAME

error_file1="compile_deps_error.txt"
error_file2="compile_server_error.txt"
escript_bin=/usr/local/bin/escript
project_path=$(pwd)
publish_root=$(pwd)
version=6.4.0
package_name=KDServer_${version}_$(date +"%Y%m%d%H%M")
publish_path=${publish_root}/${package_name}

cd ./deps
find ./ebin/ -name "*.beam"|xargs rm -rf > /dev/null
$escript_bin ../script/multi_process_compile ${error_file1}
cd ..
find ./ebin/ -name "*.beam"|xargs rm -rf > /dev/null
$escript_bin ./script/multi_process_compile ${error_file2}
cp ./deps/ebin/*.beam ./ebin/
cd $publish_root

if [ -f ${error_file1} ];then
    echo "compile deps error"
    exit -1
fi

if [ -f ${error_file2} ];then
    echo "compile error"
    exit -2
fi

if [ -d ${publish_path} ]; then
	rm -rf ${publish_path}
fi
mkdir ${publish_path}

cp -rf ${project_path}/ebin ./${package_name}
cp -rf ${project_path}/config ./${package_name}
cp -rf ${project_path}/script ./${package_name}
cp -rf ${project_path}/setting ./${package_name}
cp -rf ${project_path}/update ./${package_name}
find ./${package_name} -type d -name ".svn" |xargs rm -rvf > /dev/null;
find . -type f -name ".DS_Store" |xargs rm -rvf > /dev/null;

tar -zcvf ./${package_name}.tar.gz ./${package_name} > /dev/null
rm -rf ./${package_name} > /dev/null




