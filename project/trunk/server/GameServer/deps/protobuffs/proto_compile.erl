-module(proto_compile).

-export([scan_dir/3]).

%%--------------------------------------------------------------------
%% @doc Generates a source .erl file and header file .hrl
%%      Considered option properties: output_include_dir,
%%                                   output_ebin_dir,
%%                                   imports_dir
%%--------------------------------------------------------------------
scan_dir(SrcDir, OutHrlDir, OutDir) ->
	MsgList = filelib:fold_files(SrcDir, ".+\\.proto", false, fun(FileName, Acc) ->
		case catch parse_file_and_string(FileName) of
			{ok, TList} ->
				TList ++ Acc;
			Reason ->
				exit({Reason, FileName})
		end
	                                                          end,
		[]),
	parse_and_gen_file(MsgList, OutHrlDir, OutDir).

parse_file_and_string(FileName) ->
	{ok, TList} = parse_file(FileName),
	parse_string(TList).

parse_and_gen_file(TokenList, OutHrlDir, OutDir) ->
	check_duplicate(TokenList),
	Tokens = TokenList,
	Hrl = gen_hrl(Tokens),
	%% 用没排序过的msg列表来生成
	RouteFile = gen_route_file(TokenList),
	EncodeDef = [gen_encode_def(Tokens), encode_def_tail_clause()],
	DecodeDef = [gen_decode_def(Tokens), decode_def_tail_clause()],
	GetID = gen_get_id(Tokens),
	file:write_file(filename:join([OutHrlDir, "all_proto.hrl"]), [Hrl], [{encoding, utf8}]),
	file:write_file(filename:join([OutDir, "proto_route.erl"]), RouteFile, [{encoding, utf8}]),
	file:write_file(filename:join([OutDir, "proto_struct.erl"]), [
		"-module(proto_struct).
-include(\"all_proto.hrl\").
-compile(export_all).
-compile(nowarn_export_all).
		\r\n",
		EncodeDef, DecodeDef, GetID], [{encoding, utf8}]).

%% 判断列表中是否有重复项
check_duplicate2(List) ->
	Set = sets:from_list(List),
	case length(List) =:= sets:size(Set) of
		true ->
			ok;
		false ->
			Duplicate = List -- sets:to_list(Set),
			io:format("Duplicate=~p~n", [Duplicate]),
			exit({duplicate, Duplicate})
	end.

check_duplicate(TokenList) ->
	MsgIDList = [MsgID || {message, _MsgStr, MsgID, _Route, _FieldList} <- TokenList],
	check_duplicate2(MsgIDList),
	MsgNameList = [MsgStr || {message, MsgStr, _MsgID, _Route, _FieldList} <- TokenList],
	check_duplicate2(MsgNameList).

gen_route_file(TokenList) ->
	Header = "-module(proto_route).\n"
	"-compile(nowarn_export_all).\n"
	"-compile(export_all).\n\n",
	Body =
		lists:reverse(lists:foldl(fun({message, MsgStr, _MsgID, Route, _FieldList}, Acc) ->
			case MsgStr of
				"cs_" ++ MsgStr2 ->
					{ModStr, _} = lists:splitwith(fun(E) -> E =/= $_ end, MsgStr2),
					Server = Route,
					HandleModule = "role_" ++ ModStr,
					Case = ["route(", MsgStr, ") ->\n\t{", Server, ",", HandleModule, "};\n"],
					[Case | Acc];
				_ ->
					Acc
			end
		                          end, [], TokenList)),
	Tail = "route(_) ->undefined.\n",
	[Header, Body, Tail].

gen_hrl(String) ->
	[gen_hrl2(Name, Fields) || {message, Name, _MsgID, _Route, Fields} <- String].
gen_hrl2(Name, Fields) ->
	Head = "-record(" ++ Name ++ ",{\n\t",
	Body =
		string:join(lists:map(
			fun({_Number, _RepeatedType, _Type, FieldName, _DefaultValue}) ->
				FieldName;
				({_RepeatedType, FieldName, _FieldIndex}) ->
					atom_to_list(FieldName)
			end, Fields)
			, "\n\t,"),
	Tail = "}).\n",
	[Head, Body, Tail].

gen_encode_def(String) ->
	[gen_encode_def2(Name, MsgID, Fields) || {message, Name, MsgID, _Route, Fields} <- String].
gen_encode_def2(Name, _MsgID, Fields) ->
	Head = "encode_def(" ++ Name ++ ", R)->\n\t",
	Length = length(Fields),
	Vars = ["{_", [[",V", integer_to_list(N)] || N <- lists:seq(2, Length + 1)], "}=R,\n"],
	{_Index, FieldList} = lists:foldl(
		fun({_Number, repeated, Type, _FieldName, _DefaultValue}, {Index, AccList}) ->
			{Index + 1, ["[encode_def(" ++ Type ++ ",V0" ++ ")||V0<-" ++ "V" ++ integer_to_list(Index) ++ "]" | AccList]};
			({_Number, required, "string", _FieldName, _DefaultValue}, {Index, AccList}) ->
				{Index + 1, ["erlang:list_to_binary(" ++ "V" ++ integer_to_list(Index) ++ ")" | AccList]};
			({_Number, required, Type, _FieldName, _DefaultValue}, {Index, AccList}) ->
				{Index + 1, ["encode_def(" ++ Type ++ ",V" ++ integer_to_list(Index) ++ ")" | AccList]};
			({_RepeatedType, _FieldName, _FieldIndex}, {Index, AccList}) ->
				{Index + 1, ["V" ++ integer_to_list(Index) | AccList]}
		end, {2, []}, Fields
	),
	Body = ["[", string:join(lists:reverse(FieldList), ","), "];\n"],
	[Head, Vars, "\t", Body].

gen_get_id(String) ->
	[[["get_id(", Name, ")->", integer_to_list(MsgID), ";\n"] || {message, Name, MsgID, _Route, _Fields} <- String], "get_id(_)->0."].

encode_def_tail_clause() ->
	"encode_def(_, _) -> [].\n\n".

gen_decode_def(String) ->
	TypeIDDict = dict:from_list([{Name, MsgID} || {message, Name, MsgID, _Route, _Fields} <- String]),
	[gen_decode_def2(Name, MsgID, Fields, TypeIDDict) || {message, Name, MsgID, _Route, Fields} <- String].
gen_decode_def2(Name, MsgID, Fields, TypeIDDict) ->
	Head = "decode_def(" ++ integer_to_list(MsgID) ++ ")->\n\t[",
	Body = string:join([Name |
		lists:map(
			fun({_Number, repeated, Type, _FieldName, _DefaultValue}) ->
				"{list, " ++ integer_to_list(dict:fetch(Type, TypeIDDict)) ++ "}";
				({_Number, required, "string", FieldName, _DefaultValue}) ->
					"{string, " ++ FieldName ++ "}";
				({_Number, required, Type, _FieldName, _DefaultValue}) ->
					integer_to_list(dict:fetch(Type, TypeIDDict));
				({_RepeatedType, FieldName, _FieldIndex}) ->
					atom_to_list(FieldName)
			end, Fields
		)
	], ","),
	Tail = "];\n",
	[Head, Body, Tail].
decode_def_tail_clause() ->
	"decode_def(_) -> [].\n\n".

%% @hidden
parse_file(FileName) ->
	{ok, InFile} = protobuffs_file:open(FileName, [read]),
	String = parse_file(InFile, []),
	file:close(InFile),
	{ok, String}.

%% @hidden
parse_file(InFile, Acc) ->
	case protobuffs_file:request(InFile) of
		{ok, Token, _EndLine} ->
			parse_file(InFile, Acc ++ [Token]);
		{error, token} ->
			exit(scanning_error);
		{eof, _} ->
			Acc
	end.

parse_string(String) ->
	case lists:all(fun erlang:is_integer/1, String) of
		true ->
			{ok, Tokens, _Line} = protobuffs_scanner:string(String),
			protobuffs_parser:parse(Tokens);
		false ->
			protobuffs_parser:parse(String)
	end.