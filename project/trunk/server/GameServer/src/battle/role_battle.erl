%%%-------------------------------------------------------------------
%%% @author chenlong
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%% 战斗
%%% @end
%%% Created : 22. 三月 2019 17:50
%%%-------------------------------------------------------------------
-module(role_battle).
-author("chenlong").

-include_lib("common.hrl").

%% API
-export([cs_battle_uncover/1,cs_battle_choose/1,cs_battle_move/1,cs_battle_surrender/1,cs_battle_surrender_answer/1]).


%%=============================EXPORTED FUNCTION================================
%%翻开棋子
cs_battle_uncover(#cs_battle_uncover{stationID = StationID}) ->
	%%兵站是否合法
	case battle_lib:getCfgStation(StationID) of
		?UNDEFINED -> throw("error statioinid:"++integer_to_list(StationID));
		_ -> ok
	end,
	#role{roleID = RoleID,roomID = RoomID}=role_data:getRole(),
	EtsRoom = room_manager:getEtsRoom(RoomID),
	%%是否有房间
	%%是否在对战中
	case EtsRoom#ets_room.roomState of
		?ROOM_STATE_BATTLE -> ok;
		_ ->
			?sendself(#sc_battle_uncover{result = 1,pos = 0,stationID = 0,pieceID = 0}),
			throw(ok)
	end,
	%%自己是否是战斗位置
	#room_player{pos = Pos}=lists:keyfind(RoleID,#room_player.playerID,EtsRoom#ets_room.playerList),
	case Pos =< 2 of
		?TRUE -> ok;
		_ ->
			?sendself(#sc_battle_uncover{result = 2,pos = 0,stationID = 0,pieceID = 0}),
			throw(ok)
	end,
	EtsBattle = room_manager:getEtsBattle(RoomID),
	%%是否是回合方
	case EtsBattle#ets_battle.turnPos of
		Pos -> ok;
		_ ->
			?sendself(#sc_battle_uncover{result = 3,pos = 0,stationID = 0,pieceID = 0}),
			throw(ok)
	end,
	%%查看当前兵站信息，是否已经翻开
	Station = lists:keyfind(StationID,#station.stationID,EtsBattle#ets_battle.stationList),
	case Station#station.pieceID of
		?UNCOVER_PIECE_ID -> ok;
		_ ->
			?sendself(#sc_battle_uncover{result = 4,pos = 0,stationID = 0,pieceID = 0}),
			throw(ok)
	end,
	%%发往房间进程处理
	EtsRoom#ets_room.roomPID ! {battleUncover, RoleID, StationID}.

%%选中棋子
cs_battle_choose(#cs_battle_choose{stationID = StationID}) ->
	%%兵站是否合法
	case battle_lib:getCfgStation(StationID) of
		?UNDEFINED -> throw("error statioinid:"++integer_to_list(StationID));
		_ -> ok
	end,
	#role{roleID = RoleID,roomID = RoomID}=role_data:getRole(),
	EtsRoom = room_manager:getEtsRoom(RoomID),
	%%是否有房间
	%%是否在对战中
	case EtsRoom#ets_room.roomState of
		?ROOM_STATE_BATTLE -> ok;
		_ ->
			?sendself(#sc_battle_choose{result = 1,pos = 0,stationID = 0}),
			throw(ok)
	end,
	%%自己是否是战斗位置
	#room_player{pos = Pos}=lists:keyfind(RoleID,#room_player.playerID,EtsRoom#ets_room.playerList),
	case Pos =< 2 of
		?TRUE -> ok;
		_ ->
			?sendself(#sc_battle_choose{result = 2,pos = 0,stationID = 0}),
			throw(ok)
	end,
	EtsBattle = room_manager:getEtsBattle(RoomID),
	%%是否是回合方
	case EtsBattle#ets_battle.turnPos of
		Pos -> ok;
		_ ->
			?sendself(#sc_battle_choose{result = 3,pos = 0,stationID = 0}),
			throw(ok)
	end,
	%%要选中的兵站是否有自己的棋子
	#station{pieceID = PieceID}=lists:keyfind(StationID,#station.stationID,EtsBattle#ets_battle.stationList),
	%%是否是翻开的棋子
	CfgPiece = battle_lib:getCfgPiece(PieceID),
	case CfgPiece of
		?UNDEFINED ->
			?DEBUG("undefined piece = ~p",[PieceID]),
			throw(ok);
		_ -> ok
	end,
	PiecePos = battle_lib:getPosByPieceID(PieceID),
	case PiecePos of
		Pos -> ok;
		_ ->
			?DEBUG("yourpos=~p,piecePos=~p",[Pos,PiecePos]),
			throw(ok)
	end,
	%%是否是可移动的棋子
	case element(2,CfgPiece) of
		1 -> ok;
		_ ->
			?DEBUG("piece disMoveAble = ~p",[PieceID]),
			throw(ok)
	end,
	%%执行选中
	%%直接修改ETS字段
	ets:update_element(?ETS_BATTLE,RoomID,{#ets_battle.stationID,StationID}),
	%%广播
	role_lib:broadcastToClient(EtsRoom,#sc_battle_choose{result = 0,pos = abs(Pos),stationID = StationID}),
	ok.

%%移动
cs_battle_move(#cs_battle_move{stationID = StationID,withFlag = WithFlag0}) ->
	case lists:member(WithFlag0,[0,1]) of
		?TRUE -> ok;
		_ ->
			?DEBUG("error WithFlag0=~p",[WithFlag0]),
			throw(ok)
	end,
	WithFlag = util:int2bool(WithFlag0),
	%%兵站是否合法
	case battle_lib:getCfgStation(StationID) of
		?UNDEFINED -> throw("error statioinid:"++integer_to_list(StationID));
		_ -> ok
	end,
	#role{roleID = RoleID,roomID = RoomID}=role_data:getRole(),
	EtsRoom = room_manager:getEtsRoom(RoomID),
	%%是否有房间
	%%是否在对战中
	case EtsRoom#ets_room.roomState of
		?ROOM_STATE_BATTLE -> ok;
		_ ->
			?sendself(#sc_battle_move{result = 1,moveResult = 0, pos = 0,srcStationID = 0,tarStationID = 0,idList = []}),
			throw(ok)
	end,
	%%自己是否是战斗位置
	#room_player{pos = Pos}=lists:keyfind(RoleID,#room_player.playerID,EtsRoom#ets_room.playerList),
	case Pos =< 2 of
		?TRUE -> ok;
		_ ->
			?sendself(#sc_battle_move{result = 2,moveResult = 0, pos = 0,srcStationID = 0,tarStationID = 0,idList = []}),
			throw(ok)
	end,
	EtsBattle = room_manager:getEtsBattle(RoomID),
	%%是否是回合方
	case EtsBattle#ets_battle.turnPos of
		Pos -> ok;
		_ ->
			?sendself(#sc_battle_move{result = 3,moveResult = 0, pos = 0,srcStationID = 0,tarStationID = 0,idList = []}),
			throw(ok)
	end,
	%%是否有上次兵站信息
	case EtsBattle#ets_battle.stationID of
		0 -> throw("no last station");
		_ -> ok
	end,
	%%上次兵站信息是否是己方
	#station{stationID = LastStationID, pieceID = LastPieceID} = lists:keyfind(EtsBattle#ets_battle.stationID,#station.stationID,EtsBattle#ets_battle.stationList),
	LastPos = battle_lib:getPosByPieceID(LastPieceID),
	case LastPos of
		Pos -> ok;
		_ ->
			?DEBUG("LastPos=~p,YourPos=~p",[LastPos,Pos]),
			throw(ok)
	end,
	%%判断是否能移动过去
	{MoveResult,_} = battle_lib:stationToStation(LastStationID,StationID,EtsBattle,EtsRoom#ets_room.settingList),
	case battle_lib:canMoveByResult(MoveResult) of
		?TRUE ->
			%%发放房间进程进行处理
			EtsRoom#ets_room.roomPID ! {battleMove, RoleID,StationID,WithFlag};
		_ ->
			?sendself(#sc_battle_move{result = 4,moveResult = MoveResult, pos = 0,srcStationID = 0,tarStationID = 0,idList = []})
	end.

%%投降、求和
cs_battle_surrender(#cs_battle_surrender{type = Type}) ->
	%%验证合法性
	case lists:member(Type,[?SURRENDER_TYPE_GIVEUP,?SURRENDER_TYPE_PEACE]) of
		?TRUE -> ok;
		_ ->
			?DEBUG("error surrenderType=~p",[Type]),
			throw(ok)
	end,
	#role{roleID = RoleID,roomID = RoomID}=role_data:getRole(),
	EtsRoom = room_manager:getEtsRoom(RoomID),
	%%是否有房间
	%%是否在对战中
	case EtsRoom#ets_room.roomState of
		?ROOM_STATE_BATTLE -> ok;
		_ ->
			?sendself(#sc_battle_surrender{result = 1,cdTime = 0,pos = 0,type = Type}),
			throw(ok)
	end,
	%%自己是否是战斗位置
	#room_player{pos = Pos}=lists:keyfind(RoleID,#room_player.playerID,EtsRoom#ets_room.playerList),
	case Pos =< 2 of
		?TRUE -> ok;
		_ ->
			?sendself(#sc_battle_surrender{result = 2,cdTime = 0,pos = 0,type = Type}),
			throw(ok)
	end,
	%%是否在CD中
	EtsBattle = room_manager:getEtsBattle(RoomID),
	case EtsBattle#ets_battle.surrender of
		{Pos, _Type, SurrenderTime} ->
			Now = util:now(),
			SurrenderCD = battle_lib:getCfgBattle(surrender_cd, 10),
			case Now - SurrenderTime >= SurrenderCD of
				?TRUE -> ok;
				_ ->
					?sendself(#sc_battle_surrender{result = 3,cdTime = SurrenderTime+SurrenderCD,pos = 0,type = Type}),
					throw(ok)
			end;
		_ -> ok
	end,
	EtsRoom#ets_room.roomPID ! {battleSurrender, RoleID, Type}.

cs_battle_surrender_answer(#cs_battle_surrender_answer{isAgree = IsAgree}) ->
	#role{roleID = RoleID,roomID = RoomID}=role_data:getRole(),
	EtsRoom = room_manager:getEtsRoom(RoomID),
	%%是否有房间
	%%是否在对战中
	case EtsRoom#ets_room.roomState of
		?ROOM_STATE_BATTLE -> ok;
		_ ->
			?sendself(#sc_battle_surrender_answer{result = 1}),
			throw(ok)
	end,
	%%自己是否是战斗位置
	#room_player{pos = Pos}=lists:keyfind(RoleID,#room_player.playerID,EtsRoom#ets_room.playerList),
	case Pos =< 2 of
		?TRUE -> ok;
		_ ->
			?sendself(#sc_battle_surrender_answer{result = 2}),
			throw(ok)
	end,
	%%如果不同意，则直接忽略
	case IsAgree of
		0 ->
			?sendself(#sc_battle_surrender_answer{result = 0}),
			throw(ok);
		_ -> ok
	end,
	%%是否有对方投降记录
	EtsBattle = room_manager:getEtsBattle(RoomID),
	case EtsBattle#ets_battle.surrender of
		{OtherPos, _Type, _Time} ->
			case OtherPos of
				Pos ->
					?DEBUG("surrender OtherPos=~p,Pos=~p",[OtherPos,Pos]),
					throw(ok);
				_ -> ok
			end;
		_ ->
			?DEBUG("don't have surrender info"),
			throw(ok)
	end,
	EtsRoom#ets_room.roomPID ! {battleSurrenderAnswer, RoleID, IsAgree}.


%%=============================LOCAL FUNCTION================================