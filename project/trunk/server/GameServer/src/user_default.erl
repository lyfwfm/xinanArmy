%%%-------------------------------------------------------------------
%%% @author chenlong
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 18. 八月 2018 18:30
%%%-------------------------------------------------------------------
-module(user_default).
-author("chenlong").

-include("common.hrl").

%% API
-export([start/0,cs/0,login/1]).
-export([cl/0,pg/0,r/1,fastz/0,lc/0,lc/1,a/0]).
-export([emu/2]).
-export([t/0,t_s/0,t_p/0,destroyRoom/1]).

t() ->
	io:format("gateway = ~p~n",[ets:tab2list(?ETS_ROLE_GATEWAY)]),
	io:format("online = ~p~n",[ets:tab2list(?ETS_ROLE_ONLINE)]),
	io:format("room = ~p~n",[ets:tab2list(?ETS_ROOM)]),
	io:format("battle = ~p~n",[ets:tab2list(?ETS_BATTLE)]),
	io:format("public = ~p~n",[ets:tab2list(?ETS_ROLE_PUBLIC)]),
	ok.

start() ->
	tk:start().

cs() ->
	sample_client:start().
login(AccountID) ->
	cs(),
	timer:sleep(500),
	sample_client:login(AccountID).

%%清除log文件夹的所有日志
cl() ->
	hdlt_logger:deleteHandler(),
	filelib:fold_files("log",".+\.log",false,fun(FileName,_Acc) -> file:delete(FileName) end,ok).

%% 重新生成协议
pg() ->
	RootDir = util:root_dir(),
	proto_compile:scan_dir(filename:join([RootDir, "proto"]),filename:join([RootDir, "include"]),filename:join([RootDir, "src/proto"])).

r(Mod) ->
	R=file:delete("ebin/"++atom_to_list(Mod)++".beam"),
	io:format("delete ~p ~p~n",[Mod,R]),
	tk_misc:compile(Mod, util:root_dir()),
	c:l(Mod).
fastz() ->
	simple_make:all().

a() ->
	Fun = fun(RoleID) ->
		#role_online{socket = Socket}=role_lib:getRoleOnline(RoleID),
		#ets_role_public{roleName = RoleName}=role_lib:getRolePublic(RoleID),
		show(RoleName),
		case inet:peername(Socket) of
			{ok,{Addr,_}} -> io:format("\t\t~w",[Addr]);
			_ -> ok
		end,
		io:format("\t\t~w~n",[RoleID])
		end,
	util:ets_foreach_key(Fun, ?ETS_ROLE_ONLINE).

%%加载配置
lc()->
	tk_config:preload_config().
lc(A) ->
	tk_config:reload_config(A).

%%通过玩家进程发消息
emu(RoleID, Msg) ->
	Record = Msg,
	case proto_route:route(element(1, Record)) of
		{role, HandleModule} ->
			RolePID = util:getEtsElement(?ETS_ROLE_ONLINE, RoleID, #role_online.rolePID, ?UNDEFINED),
			?CATCH(erlang:send(RolePID, {client_msg, HandleModule, Record}));
		{Server, _HandleModule} ->
			catch erlang:send(Server, {client_msg, RoleID, Record})
	end.

show(Str0) ->
	Str = ?IF(is_binary(Str0),binary_to_list(Str0),Str0),
	io:format("~ts",[unicode:characters_to_binary(Str,utf8,latin1)]).

%%检测兵站配置
t_s() ->
	try
	CheckFun = fun(ID) ->
		try
		{Type,HIDList, VIDList}=data_station:get(ID),
		%%检测连通个数，是否正确
		R = case Type of
			?STATION_TYPE_XING_YING ->
				length(HIDList)=:=2 andalso length(VIDList)=:=6;
			?STATION_TYPE_KUAI_SU_BING_ZHAN ->
				case length(HIDList) of
					1 -> lists:member(length(VIDList),[2,3,4]);
					2 -> lists:member(length(VIDList),[1,2,4])
				end;
			?STATION_TYPE_BING_ZHAN ->
				case length(HIDList) of
					1 -> length(VIDList)=:=1;
					2 -> length(VIDList)=:=1 orelse length(VIDList)=:=2
				end;
			?STATION_TYPE_DA_BEN_YING ->
				length(HIDList)=:=2 andalso length(VIDList)=:=1;
			?STATION_TYPE_QIAN_XIAN ->
				case length(HIDList) of
					1 -> length(VIDList)=:=2;
					2 -> length(VIDList)=:=2
				end;
			?STATION_TYPE_SHAN_JIE ->
				length(HIDList)=:=2 andalso length(VIDList)=:=0
		end,
		case R of
			?TRUE -> ok;
			_ -> throw(num)
		end,
		%%检测是否连通有你，你中有我
			Fun2 = fun(ID1,Pos) ->
				case lists:member(ID,element(Pos,data_station:get(ID1))) of
					?TRUE -> ok;
					_ -> throw(io_lib:format("ID1=~p",[ID1]))
				end
				end,
			[Fun2(ID1,2)||ID1<-HIDList],
			[Fun2(ID2,3)||ID2<-VIDList]
		catch
			_:Why -> io:format("Why=~p~n",[Why]),throw(ID)
		end
		end,
	lists:foreach(CheckFun,data_station:get_list()),
	success
	catch
		_:ErrorID -> ErrorID
	end.
%%检测棋子配置
t_p() ->
	try
	ok
	catch
		_:Why -> Why
	end.
destroyRoom(RoomID) ->
	#ets_room{roomPID = RoomPID}=room_manager:getEtsRoom(RoomID),
	gen_server:cast(RoomPID,{destroy}).

%%----------------------