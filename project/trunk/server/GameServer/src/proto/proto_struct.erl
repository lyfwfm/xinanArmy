-module(proto_struct).
-include("all_proto.hrl").
-compile(export_all).
-compile(nowarn_export_all).
		
encode_def(cs_chat_send, R)->
	{_,V2,V3}=R,
	[V2,V3];
encode_def(sc_chat_send, R)->
	{_,V2,V3}=R,
	[V2,V3];
encode_def(sc_chat_content, R)->
	{_,V2,V3,V4,V5}=R,
	[V2,V3,erlang:list_to_binary(V4),erlang:list_to_binary(V5)];
encode_def(sc_battle_begin, R)->
	{_}=R,
	[];
encode_def(sc_battle_turn, R)->
	{_,V2,V3}=R,
	[V2,V3];
encode_def(cs_battle_uncover, R)->
	{_,V2}=R,
	[V2];
encode_def(sc_battle_uncover, R)->
	{_,V2,V3,V4,V5}=R,
	[V2,V3,V4,V5];
encode_def(cs_battle_choose, R)->
	{_,V2}=R,
	[V2];
encode_def(sc_battle_choose, R)->
	{_,V2,V3,V4}=R,
	[V2,V3,V4];
encode_def(cs_battle_move, R)->
	{_,V2,V3}=R,
	[V2,V3];
encode_def(sc_battle_move, R)->
	{_,V2,V3,V4,V5,V6,V7}=R,
	[V2,V3,V4,V5,V6,V7];
encode_def(sc_battle_piece_dead, R)->
	{_,V2}=R,
	[V2];
encode_def(sc_battle_over, R)->
	{_,V2,V3,V4}=R,
	[V2,erlang:list_to_binary(V3),erlang:list_to_binary(V4)];
encode_def(cs_battle_surrender, R)->
	{_,V2}=R,
	[V2];
encode_def(sc_battle_surrender, R)->
	{_,V2,V3,V4,V5}=R,
	[V2,V3,V4,V5];
encode_def(cs_battle_surrender_answer, R)->
	{_,V2}=R,
	[V2];
encode_def(sc_battle_surrender_answer, R)->
	{_,V2}=R,
	[V2];
encode_def(sc_battle_info, R)->
	{_,V2,V3,V4,V5,V6,V7}=R,
	[V2,V3,V4,V5,V6,V7];
encode_def(sc_battle_camp, R)->
	{_,V2,V3}=R,
	[V2,V3];
encode_def(cs_room_list, R)->
	{_,V2}=R,
	[V2];
encode_def(p_room_member, R)->
	{_,V2,V3,V4,V5}=R,
	[V2,V3,erlang:list_to_binary(V4),erlang:list_to_binary(V5)];
encode_def(p_roomInfo, R)->
	{_,V2,V3,V4,V5,V6,V7,V8}=R,
	[V2,erlang:list_to_binary(V3),V4,V5,V6,encode_def(p_room_member,V7),encode_def(p_room_member,V8)];
encode_def(sc_room_list, R)->
	{_,V2}=R,
	[[encode_def(p_roomInfo,V0)||V0<-V2]];
encode_def(cs_room_enter, R)->
	{_,V2,V3}=R,
	[V2,V3];
encode_def(sc_room_enter, R)->
	{_,V2}=R,
	[V2];
encode_def(cs_room_create, R)->
	{_,V2,V3,V4}=R,
	[V2,V3,V4];
encode_def(sc_room_create, R)->
	{_,V2}=R,
	[V2];
encode_def(sc_room_short_info, R)->
	{_,V2,V3,V4,V5,V6,V7,V8}=R,
	[[encode_def(p_room_member,V0)||V0<-V2],V3,V4,erlang:list_to_binary(V5),erlang:list_to_binary(V6),V7,V8];
encode_def(cs_room_change_rule, R)->
	{_,V2}=R,
	[V2];
encode_def(sc_room_change_rule, R)->
	{_,V2,V3}=R,
	[V2,V3];
encode_def(cs_room_change_pos, R)->
	{_,V2}=R,
	[V2];
encode_def(sc_room_change_pos, R)->
	{_,V2,V3,V4,V5}=R,
	[V2,V3,V4,V5];
encode_def(cs_room_change_leader, R)->
	{_,V2}=R,
	[V2];
encode_def(sc_room_change_leader, R)->
	{_,V2,V3,V4}=R,
	[V2,V3,V4];
encode_def(cs_room_kick, R)->
	{_,V2}=R,
	[V2];
encode_def(sc_room_kick, R)->
	{_,V2,V3,V4}=R,
	[V2,V3,V4];
encode_def(cs_room_leave, R)->
	{_}=R,
	[];
encode_def(sc_room_leave, R)->
	{_,V2,V3,V4}=R,
	[V2,V3,V4];
encode_def(cs_room_change_ready, R)->
	{_}=R,
	[];
encode_def(sc_room_change_ready, R)->
	{_,V2,V3,V4}=R,
	[V2,V3,V4];
encode_def(sc_room_member_list, R)->
	{_,V2,V3}=R,
	[[encode_def(p_room_member,V0)||V0<-V2],V3];
encode_def(cs_room_fast_join, R)->
	{_,V2}=R,
	[V2];
encode_def(sc_room_fast_join, R)->
	{_,V2}=R,
	[V2];
encode_def(cs_room_modify_room, R)->
	{_,V2,V3,V4}=R,
	[erlang:list_to_binary(V2),erlang:list_to_binary(V3),V4];
encode_def(sc_room_modify_room, R)->
	{_,V2,V3,V4,V5}=R,
	[V2,erlang:list_to_binary(V3),erlang:list_to_binary(V4),V5];
encode_def(cs_role_login, R)->
	{_,V2,V3,V4,V5}=R,
	[V2,V3,V4,V5];
encode_def(sc_role_login, R)->
	{_,V2,V3}=R,
	[V2,V3];
encode_def(cs_role_heart_beat, R)->
	{_}=R,
	[];
encode_def(sc_role_heart_beat, R)->
	{_,V2}=R,
	[V2];
encode_def(_, _) -> [].

decode_def(10301)->
	[cs_chat_send,channel,content];
decode_def(10302)->
	[sc_chat_send,result,cdEndTime];
decode_def(10303)->
	[sc_chat_content,channel,roleID,{string, roleName},{string, content}];
decode_def(10201)->
	[sc_battle_begin];
decode_def(10202)->
	[sc_battle_turn,pos,turnEndTime];
decode_def(10203)->
	[cs_battle_uncover,stationID];
decode_def(10204)->
	[sc_battle_uncover,result,pos,stationID,pieceID];
decode_def(10205)->
	[cs_battle_choose,stationID];
decode_def(10206)->
	[sc_battle_choose,result,pos,stationID];
decode_def(10207)->
	[cs_battle_move,stationID,withFlag];
decode_def(10208)->
	[sc_battle_move,result,moveResult,pos,srcStationID,tarStationID,idList];
decode_def(10209)->
	[sc_battle_piece_dead,pieceIDList];
decode_def(10210)->
	[sc_battle_over,winPos,{string, playerName},{string, playerHead}];
decode_def(10211)->
	[cs_battle_surrender,type];
decode_def(10212)->
	[sc_battle_surrender,result,cdTime,pos,type];
decode_def(10213)->
	[cs_battle_surrender_answer,isAgree];
decode_def(10214)->
	[sc_battle_surrender_answer,result];
decode_def(10215)->
	[sc_battle_info,stationInfoList,flag1StationID,flag2StationID,deadPieceList,turnPos,turnEndTime];
decode_def(10216)->
	[sc_battle_camp,playerID1,playerID2];
decode_def(10101)->
	[cs_room_list,roomID];
decode_def(10102)->
	[p_room_member,pos,playerID,{string, playerName},{string, playerHead}];
decode_def(10103)->
	[p_roomInfo,roomID,{string, roomName},isSec,watchingNum,settingList,10102,10102];
decode_def(10104)->
	[sc_room_list,{list, 10103}];
decode_def(10105)->
	[cs_room_enter,roomID,roomSec];
decode_def(10106)->
	[sc_room_enter,result];
decode_def(10107)->
	[cs_room_create,roomName,roomSec,settingList];
decode_def(10108)->
	[sc_room_create,result];
decode_def(10109)->
	[sc_room_short_info,{list, 10102},leaderID,settingList,{string, roomName},{string, roomSec},roomState,roomID];
decode_def(10110)->
	[cs_room_change_rule,settingList];
decode_def(10111)->
	[sc_room_change_rule,result,settingList];
decode_def(10112)->
	[cs_room_change_pos,pos];
decode_def(10113)->
	[sc_room_change_pos,result,playerID,srcPos,tarPos];
decode_def(10114)->
	[cs_room_change_leader,playerID];
decode_def(10115)->
	[sc_room_change_leader,result,srcPlayerID,tarPlayerID];
decode_def(10116)->
	[cs_room_kick,playerID];
decode_def(10117)->
	[sc_room_kick,result,playerID,pos];
decode_def(10118)->
	[cs_room_leave];
decode_def(10119)->
	[sc_room_leave,result,playerID,pos];
decode_def(10120)->
	[cs_room_change_ready];
decode_def(10121)->
	[sc_room_change_ready,result,playerID,readyState];
decode_def(10122)->
	[sc_room_member_list,{list, 10102},roomState];
decode_def(10123)->
	[cs_room_fast_join,settingList];
decode_def(10124)->
	[sc_room_fast_join,result];
decode_def(10125)->
	[cs_room_modify_room,{string, roomName},{string, roomSec},settingList];
decode_def(10126)->
	[sc_room_modify_room,result,{string, roomName},{string, roomSec},settingList];
decode_def(10001)->
	[cs_role_login,code,roleName,roleHead,roomID];
decode_def(10002)->
	[sc_role_login,result,roleID];
decode_def(10003)->
	[cs_role_heart_beat];
decode_def(10004)->
	[sc_role_heart_beat,serverTime];
decode_def(_) -> [].

get_id(cs_chat_send)->10301;
get_id(sc_chat_send)->10302;
get_id(sc_chat_content)->10303;
get_id(sc_battle_begin)->10201;
get_id(sc_battle_turn)->10202;
get_id(cs_battle_uncover)->10203;
get_id(sc_battle_uncover)->10204;
get_id(cs_battle_choose)->10205;
get_id(sc_battle_choose)->10206;
get_id(cs_battle_move)->10207;
get_id(sc_battle_move)->10208;
get_id(sc_battle_piece_dead)->10209;
get_id(sc_battle_over)->10210;
get_id(cs_battle_surrender)->10211;
get_id(sc_battle_surrender)->10212;
get_id(cs_battle_surrender_answer)->10213;
get_id(sc_battle_surrender_answer)->10214;
get_id(sc_battle_info)->10215;
get_id(sc_battle_camp)->10216;
get_id(cs_room_list)->10101;
get_id(p_room_member)->10102;
get_id(p_roomInfo)->10103;
get_id(sc_room_list)->10104;
get_id(cs_room_enter)->10105;
get_id(sc_room_enter)->10106;
get_id(cs_room_create)->10107;
get_id(sc_room_create)->10108;
get_id(sc_room_short_info)->10109;
get_id(cs_room_change_rule)->10110;
get_id(sc_room_change_rule)->10111;
get_id(cs_room_change_pos)->10112;
get_id(sc_room_change_pos)->10113;
get_id(cs_room_change_leader)->10114;
get_id(sc_room_change_leader)->10115;
get_id(cs_room_kick)->10116;
get_id(sc_room_kick)->10117;
get_id(cs_room_leave)->10118;
get_id(sc_room_leave)->10119;
get_id(cs_room_change_ready)->10120;
get_id(sc_room_change_ready)->10121;
get_id(sc_room_member_list)->10122;
get_id(cs_room_fast_join)->10123;
get_id(sc_room_fast_join)->10124;
get_id(cs_room_modify_room)->10125;
get_id(sc_room_modify_room)->10126;
get_id(cs_role_login)->10001;
get_id(sc_role_login)->10002;
get_id(cs_role_heart_beat)->10003;
get_id(sc_role_heart_beat)->10004;
get_id(_)->0.