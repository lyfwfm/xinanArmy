-module(proto_route).
-compile(nowarn_export_all).
-compile(export_all).

route(cs_chat_send) ->
	{role,role_chat};
route(cs_battle_uncover) ->
	{role,role_battle};
route(cs_battle_choose) ->
	{role,role_battle};
route(cs_battle_move) ->
	{role,role_battle};
route(cs_battle_surrender) ->
	{role,role_battle};
route(cs_battle_surrender_answer) ->
	{role,role_battle};
route(cs_room_list) ->
	{role,role_room};
route(cs_room_enter) ->
	{role,role_room};
route(cs_room_create) ->
	{role,role_room};
route(cs_room_change_rule) ->
	{role,role_room};
route(cs_room_change_pos) ->
	{role,role_room};
route(cs_room_change_leader) ->
	{role,role_room};
route(cs_room_kick) ->
	{role,role_room};
route(cs_room_leave) ->
	{role,role_room};
route(cs_room_change_ready) ->
	{role,role_room};
route(cs_room_fast_join) ->
	{role,role_room};
route(cs_room_modify_room) ->
	{role,role_room};
route(cs_role_login) ->
	{role,role_role};
route(cs_role_heart_beat) ->
	{role,role_role};
route(_) ->undefined.
