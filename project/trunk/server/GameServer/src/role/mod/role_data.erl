%%%-------------------------------------------------------------------
%%% @author chenlong
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%% 玩家数据相关
%%% @end
%%% Created : 21. 三月 2019 13:57
%%%-------------------------------------------------------------------
-module(role_data).
-author("chenlong").

-include("common.hrl").

%% API
-export([getRole/0,setRole/1,getRoleID/0]).

%%=============================EXPORTED FUNCTION====================================
getRole() ->
	get(?ROLE_ROLE).
setRole(#role{}=R) ->
	put(?ROLE_ROLE,R).
getRoleID() ->
	R = getRole(),
	R#role.roleID.