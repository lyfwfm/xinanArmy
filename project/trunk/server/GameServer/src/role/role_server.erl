%%%-------------------------------------------------------------------
%%% @author chenlong
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%% 玩家公共信息进程
%%% @end
%%% Created : 17. 八月 2018 11:42
%%%-------------------------------------------------------------------
-module(role_server).
-author("chenlong").

-behaviour(gen_server).

-include("common.hrl").
-include_lib("stdlib/include/ms_transform.hrl").

%% API
-export([start_link/1]).

%% gen_server callbacks
-export([init/1,
	handle_call/3,
	handle_cast/2,
	handle_info/2,
	terminate/2,
	code_change/3]).


-define(SERVER, ?MODULE).

-record(state, {}).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @end
%%--------------------------------------------------------------------
start_link(Args) ->
	gen_server:start_link(?MODULE, [Args], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
-spec(init(Args :: term()) ->
	{ok, State :: #state{}} | {ok, State :: #state{}, timeout() | hibernate} |
	{stop, Reason :: term()} | ignore).
init([{SendToSocketFun, Socket} | _]) ->
	put(?SEND_TO_SOCKET_FUN, SendToSocketFun),
	put(?ROLE_SOCKET, Socket),
	process_flag(trap_exit, true),
	timer_wheel:init(),
	{ok, #state{}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
	State :: #state{}) ->
	{reply, Reply :: term(), NewState :: #state{}} |
	{reply, Reply :: term(), NewState :: #state{}, timeout() | hibernate} |
	{noreply, NewState :: #state{}} |
	{noreply, NewState :: #state{}, timeout() | hibernate} |
	{stop, Reason :: term(), Reply :: term(), NewState :: #state{}} |
	{stop, Reason :: term(), NewState :: #state{}}).
handle_call(_Request, _From, State) ->
	{reply, ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_cast(Request :: term(), State :: #state{}) ->
	{noreply, NewState :: #state{}} |
	{noreply, NewState :: #state{}, timeout() | hibernate} |
	{stop, Reason :: term(), NewState :: #state{}}).
handle_cast({gateway_down}, State) ->
	{stop, normal, State};
handle_cast({login_again, FromPID, Params}, State) ->
	%%重复登录了，下线
	{stop, {login_again, FromPID, Params}, State};
handle_cast(_Request, State) ->
	{noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #state{}) ->
	{noreply, NewState :: #state{}} |
	{noreply, NewState :: #state{}, timeout() | hibernate} |
	{stop, Reason :: term(), NewState :: #state{}}).
handle_info(Info, State) ->
	try
		case Info of
			{?timer_wheel_tick, Sec} ->
				timer_wheel:work(Sec);
			{client_msg, HandleModule, Record} ->
				FuncName = element(1, Record),
				?CATCH(HandleModule:FuncName(Record));
			{offlineEvent} ->
				role_offline:onOfflineEvent();
			{resCreateRoom, RoomID} ->
				role_room:onResCreateRoom(RoomID);
			{resEnterRoom, EtsRoom} ->
				role_room:onResEnterRoom(EtsRoom);
			{roomKick} ->
				role_room:onRoomKick();
			{resLeaveRoom, RoomID} ->
				role_room:onResLeaveRoom(RoomID);

			{res_login_again, {RoleID, RoleName, RoleHead,InitRoomID}} ->
				role_role:doPlayerLogin_1(RoleID, RoleName, RoleHead,InitRoomID);
			_ ->
				?ERR("no match Info=~p", [Info])
		end
	catch
		_:Why:StackTrace ->
			?ERR("role_server handle_info Info=~p,Why=~p,StackTrace=~p", [Info, Why, StackTrace])
	end,
	{noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
	State :: #state{}) -> term()).
terminate(Reason, _State) ->
	try
		RoleID = role_data:getRoleID(),
		?INFO("role_server shutdown RoleID = ~p, SendToSocketFun = ~p, Reason=~p", [RoleID, get(?SEND_TO_SOCKET_FUN), Reason]),
		?CATCH(erlang:unregister(util:roleRegName(RoleID))),
		ets:delete(?ETS_ROLE_ONLINE, RoleID),
		%%保存数据库
		db_sql:setRole(role_data:getRole()),
		case Reason of
			{login_again, FromPID, Params} ->
				FromPID ! {res_login_again, Params},
				?sendself(#sc_role_login{result = 1,roleID = RoleID}),
				ok;
			_ -> ok
		end,
		ok
	catch
		_:_ -> ok
	end,
	ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
-spec(code_change(OldVsn :: term() | {down, term()}, State :: #state{},
	Extra :: term()) ->
	{ok, NewState :: #state{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

%%%===================================================================
%%% exported functions
%%%===================================================================


%%%===================================================================
%%% Internal functions
%%%===================================================================