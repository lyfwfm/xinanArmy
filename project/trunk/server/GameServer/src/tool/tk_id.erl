%% @doc 服务器所有ID生成相关接口
%% Alarm : 请确保合服时不用修改ID

%%临时ID，需要自己进程保证不会并发获取和释放


-module(tk_id).
-include("common.hrl").
-export([init/0]).
-export([gen_roleID/0,is_robot/1,getServerIDByRoleID/1]).
-export([gen_roomID/0]).

%% ====================================================================
%% API functions
%% ====================================================================
init() ->
	init_id(roleID,"roleID","grole",?ROLE_ID_BASE),
	init_temp_id(roomID),
    ok.

%% @doc 生成玩家ID
gen_roleID() ->
	ets:update_counter(?ETS_ID, roleID, 1).

gen_roomID() ->
	ets:update_counter(?ETS_ID,roomID,1).

	
%% ====================================================================
%% Internal functions
%% ====================================================================
init_id(IDName, Key,Table,Base) ->
	ID = init_id2(Key,Table,Base),
	ets:insert(?ETS_ID, {IDName, ID}).

init_id2(Key,Table,Base) ->
	Sql = io_lib:format("select max(~s) from ~s;",[Key,Table]),
	case db_sql:get_row(Sql) of
		[Max] when is_integer(Max)->
			next;
		_ ->
			Max=0
	end,
	erlang:max(Max, Base).

%% @doc 判断是否是机器人ID
is_robot(RoleID) ->
	RoleID < ?ROLE_ID_BASE.

getServerIDByRoleID(RoleID) ->
	RoleID div ?SPACE_ID_NUMBER.

init_temp_id(IDName) ->
	ets:insert(?ETS_ID,{IDName,0}).