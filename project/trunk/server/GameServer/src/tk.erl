-module(tk).

-export([start/0,stop/0]).

-include("common.hrl").
-define(APPS,[
	sasl, kernel, stdlib, crypto, inets, asn1, public_key, ssl, compiler, xmerl, syntax_tools, jsx, emysql, db,
	mochiweb, ejson, basic,
	world, role]).

start() ->
	user_default:lc(),
	case update_db_util:update_db() of
		?TRUE ->
			tk_misc:start_applications(?APPS),
			%%检查代码是否有冲突
			code:clash(),
			ok;
		_ ->
			case data_setting:get(is_release) of
				?TRUE ->
					erlang:halt(1);
				_ -> ok
			end,
			fail
	end.

stop() ->
	StopFunc = fun(App) ->
		application:stop(App)
		end,
	lists:foreach(StopFunc,[role,world,basic,mochiweb]).