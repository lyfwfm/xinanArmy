-include("role.hrl").
-include("room.hrl").
-include("battle.hrl").
-include("offlineEvent.hrl").
-include("chat.hrl").

-define(ETS_ROLE_ONLINE, ets_role_online).%%进程保存所有在线玩家信息的ETS
-define(ETS_ROLE_GATEWAY, ets_role_gateway).%%ETS 玩家网关信息
-define(ETS_ROLE_PUBLIC, ets_role_public).%%玩家public信息，维护一个从数据库加载的ETS


-define(ETS_ID,ets_id).% 生成ID的ets表

-define(SPACE_ID_NUMBER, 1000000).%%预留100W
-define(ROLE_ID_BASE, ((data_setting:get(server_id)) * ?SPACE_ID_NUMBER)).%%预留100W

-define(SEND_TO_SOCKET_FUN, sendToSocketFun).
-define(ROLE_SOCKET, role_socket).
-define(sendself(Msg), role_lib:sendToMyClient(Msg)).

-define(timer_wheel_tick, timer_wheel_tick).
-define(DEFAULT_TIMEOUT, 5000).

-define(ONE_HOUR_SECONDS, 3600).
-define(ONE_DAY_SECONDS, 86400).
-define(ONE_WEEK_SECONDS, 604800).

-define(ROLE_PUBLIC_DELETE_TIME, ?ONE_WEEK_SECONDS).
-define(HEART_BEAT_TIME_OUT, 1800).