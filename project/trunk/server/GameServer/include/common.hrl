%%%-------------------------------------------------------------------
%%% @author chenlong
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 15. 八月 2018 17:32
%%%-------------------------------------------------------------------
-author("chenlong").
-include("cfg_record.hrl").
-include("all_proto.hrl").
-include("common_define.hrl").
-include("common_record.hrl").

-define(UNDEFINED, undefined).
-define(DB, tk).

-define(CATCH(Expression), (
	try Expression
	catch
		throw:ok -> ok;
		throw:Reason:Stack -> ?WARN("Reason=~p,Stack=~p",[Reason,Stack]);
		ErrType:Reason:Stack ->
			?ERR("ErrType:~1000p, ErrReason:~1000p, Stack=~p, Expression=~s",[ErrType, Reason, Stack, ??Expression]),
			{'EXIT',{ErrType, Reason}}
	end
)).
-define(TRUE, true).
-define(FALSE, false).
-define(ETS_CONCURRENCY, {read_concurrency, true}).
-define(IF(TOrF, True, False), case TOrF of ?TRUE -> True; _ -> False end).


%%-------------------------------------------------------------------------
%% 信息
-define(INFO(Format), hdlt_logger:log("[~p:~p]"++Format,[?MODULE,?LINE])).
-define(INFO(Format, Args), hdlt_logger:log("[~p:~p]"++Format,[?MODULE,?LINE]++Args)).

%% 调试信息
-define(DEBUG(Format), hdlt_logger:debug("[~p:~p]"++Format,[?MODULE,?LINE])).
-define(DEBUG(Format, Args), hdlt_logger:debug("[~p:~p]"++Format,[?MODULE,?LINE]++Args)).

%% 警告信息
-define(WARN(Format), hdlt_logger:warn("[~p:~p]"++Format,[?MODULE,?LINE])).
-define(WARN(Format, Args), hdlt_logger:warn("[~p:~p]"++Format,[?MODULE,?LINE]++Args)).

%% 错误信息
-define(ERR(Format), hdlt_logger:error_out("[~p:~p]"++Format,[?MODULE,?LINE])).
-define(ERR(Format, Args), hdlt_logger:error_out("[~p:~p]"++Format,[?MODULE,?LINE]++Args)).
%%-------------------------------------------------------------------------
