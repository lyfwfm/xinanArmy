
%%玩家进程保存的玩家数据
-record(role,{
	roleID=0,%%玩家唯一ID
	roleName="",
	headurl = "",%%头像网址
	roomID=0,%%玩家进程临时存储的，所在房间ID
	score = 0%%积分
}).
%%玩家进程保存的聊天数据
-record(chat,{
	cdList = []%%CD信息{Channel, CDEndTimestamp}
}).

%%玩家网关信息
-record(role_gateway,{
	roleGWPID=0,%%玩家网络通信层的进程PID
	rolePID=0,%%玩家进程PID
	roleID=0,
	sendToSocketFun,
	heartBeatTime=0
}).

%%玩家在线信息
-record(role_online,{
	roleID=0,
	sendToSocketFun,
	socket,
	rolePID
}).

%%玩家public信息，维护一个从数据库加载的ETS
-record(ets_role_public,{
	roleID=0,
	roleName="",
	headurl="",
	lastRefTime=0%%上次访问的时间戳，作为加载、删除的时间依据
}).