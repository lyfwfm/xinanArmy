%%%-------------------------------------------------------------------
%%% @author chenlong
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 21. 三月 2019 15:32
%%%-------------------------------------------------------------------
-author("chenlong").

-define(HORIZON, horizon).%%横向
-define(VERTICAL, vertical).%%纵向
-define(ALL, all).%%全方向

%%房间设置列表，包含战斗规则
-define(SETTING_TYPE_1, 1).%%工兵是否可以到山界
%%-define(SETTING_TYPE_2, 2).%%能否带军旗进行营
%%-define(SETTING_TYPE_3, 3).%%自己在军旗上，吃人是否需要下军旗
%%-define(SETTING_TYPE_4, 4).%%工兵是否可以背军旗到山界
-define(SETTING_TYPE_5, 5).%%对方地雷未排完，是否可以吃在大本营里面的棋子
-define(SETTING_TYPE_6, 6).%%炸弹是否可以炸军旗上面的棋子
%%-define(SETTING_TYPE_7, 7).%%地雷在大本营翻开，是否自爆
-define(SETTING_MAX_LEN, 7).%%规定一个规则的长度，来验证客户端发来的合法性

%%棋子军衔
-define(UNCOVER_PIECE_ID, -8).%%未翻开棋子
-define(PIECE_LEVEL_GONG_BING, 9).%%工兵
-define(PIECE_LEVEL_ZHA_DAN, 10).%%炸弹
-define(PIECE_LEVEL_DI_LEI, 11).%%地雷
-define(PIECE_LEVEL_JUN_QI, 12).%%军旗

%%兵站类型
-define(STATION_TYPE_XING_YING, 1).
-define(STATION_TYPE_KUAI_SU_BING_ZHAN, 2).
-define(STATION_TYPE_BING_ZHAN, 3).
-define(STATION_TYPE_DA_BEN_YING, 4).
-define(STATION_TYPE_QIAN_XIAN, 5).
-define(STATION_TYPE_SHAN_JIE, 6).

%%投降求和
-define(SURRENDER_TYPE_GIVEUP, 1).%%投降
-define(SURRENDER_TYPE_PEACE, 2).%%求和