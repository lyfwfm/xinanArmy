%%%-------------------------------------------------------------------
%%% @author chenlong
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 21. 三月 2019 13:40
%%%-------------------------------------------------------------------
-author("chenlong").

-define(ETS_ROOM, ets_room).
-define(ETS_BATTLE, ets_battle).

-define(ROOM_STATE_IDLE, 1).
-define(ROOM_STATE_DESTROY, 2).%%正在销毁状态
-define(ROOM_STATE_BATTLE, 3).%%对战中

-define(PLAYER_STATE_IDLE, 1).
-define(PLAYER_STATE_READY, 2).%%准备状态

%%room信息
-record(ets_room,{
	roomID=0,
	masterID=0,
	roomPID=0,
	roomState=?ROOM_STATE_IDLE,%%房间状态
	roomName="",%%房间名称
	roomSec="",%%房间密码
	settingList=[],%%战斗规则列表[Value] 通过序号来判断是哪个规则
	playerList=[],%%房友信息#room_player{}
	%%fakeRoom属性
	isFake=false,
	endTimestamp=0%%生存周期结束时间戳
}).


%%--------------------战斗相关----------------------------
-record(station,{
	stationID=0,
	pieceID=0,%%在兵站上的棋子ID -8表示有未翻开棋子
	flagPieceList=[]%%里面记录当前军旗ID 棋子上军旗时，将军旗ID放入列表，下军旗时将军旗ID重新填回上面PieceID
}).
%%阵亡信息
-record(dead,{
	pieceID=0,
	byPieceID=0%%被哪个棋子干掉的
}).
%%战斗信息，以房间为单位
-record(ets_battle,{
	roomID=0,
	turnPos=0,%%当前回合方
	turnEndTime=0,%%回合结束时间戳
	stationID=0,%%当前回合方选中的兵站（上面的棋子），如果没有，则表示第一次点击
	pieceIDList=[],%%当前剩余未翻开的棋子ID列表，战斗开始时要初始化这个列表
	stationList=[],%%当前棋盘数据#station{}
	deadList={[],[]},%%阵亡数据{Pos1DeadList,Pos2DeadList}::#dead{}
	systemTurnCount={0,0},%%因为时间到，系统自动切换回合的次数 元组对应1/2号位
	lastTurnTimeOut={false,false},%%上个回合是否是系统自动切换的回合，用来判断这个回合是否进入紧急烧绳子时间 元组对应1/2号位
	lastPiecePos=0,%%上一次翻开的棋子，所属红方还是蓝方，用来作为决定红蓝方的依据
	surrender={}%%投降求和信息 {Pos, Type, SurrenderTime}%%SurrenderTime为发起投降的时间戳
}).


%%-------------room进程-----------------
-define(ROOM_ROOM, room_room).
%%room进程里面，自己存储room的信息
-record(room,{
	roomID=0
}).
%%房友信息
-record(room_player,{
	playerID=0,
	pos=0,%%游戏开始时，将对战双方Pos置为-1;-2 表示此时玩家所属红蓝方还未定下来
	state=?PLAYER_STATE_IDLE
}).