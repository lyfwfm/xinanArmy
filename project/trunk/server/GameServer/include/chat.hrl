%%%-------------------------------------------------------------------
%%% @author chenlong
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 28. 三月 2019 14:19
%%%-------------------------------------------------------------------
-author("chenlong").

-define(ROLE_CHAT, role_chat).%%进程字典存储#chat{}

-define(CHAT_CHANNEL_TYPE_SYSTEM, 1).
-define(CHAT_CHANNEL_TYPE_WORLD, 2).
-define(CHAT_CHANNEL_TYPE_ROOM, 3).