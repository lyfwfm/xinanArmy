DROP TABLE IF EXISTS `gAccount`;
CREATE TABLE `gAccount` (
  `roleID` int(11) unsigned NOT NULL COMMENT '玩家ID',
  `openid` varchar(200) NOT NULL COMMENT 'wx的openid',
  PRIMARY KEY (`roleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `gRole`;
CREATE TABLE `gRole` (
  `roleID` int(11) unsigned NOT NULL COMMENT '玩家ID',
  `roleName` varchar(200) NOT NULL COMMENT '玩家名字',
  `roleHead` varchar(500) NOT NULL COMMENT '玩家头像地址',
  `score` smallint(5) unsigned NOT NULL COMMENT '积分',
  PRIMARY KEY (`roleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `gOfflineEvent`;
CREATE TABLE `gOfflineEvent` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `roleID` int(11) unsigned NOT NULL COMMENT '玩家ID',
  `eventData` blob NOT NULL COMMENT '离线事件数据',
  PRIMARY KEY (`id`),
  KEY `roleID` (`roleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;