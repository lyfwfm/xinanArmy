客户端给服务器发消息 
	格式：对象形式 第一个为协议ID (这里对象就自己构造，因为客户端发送的消息，一般不复杂)
		javascript	JSON.stringify([101,100,[1,2,3]])
	服务器收到消息 解析为 [101,100,[1,2,3]] 列表  (这里处理函数就直接处理，因为客户端发送的消息，一般不复杂)
		erlang	mochijson2:decode(Payload)
		
服务器给客户端发消息
	格式：(发送前，简化成列表这种对象形式，todo 需要一个函数从record到列表对象)
		erlang	mochijson2:encode([100,"your name",[[101,"101"],[102,"102"]]])
	客户端收到消息 解析为对象 通过 对象[Index] 来访问不同字段
		javascript	JSON.parse(event.data)
		
协议范例：
如果字段类型为基本类型则不用写类型，如果自定义类型，则需要给出自定义类型名称(eg:p_duiplicate)
message	sc_role_test[id=10003]{
	required	myid=1;
	repeated	p_duiplicate	msglist=2;
}

