
import * as globalObj from './proto_lib';

var roomID=0;//临时存储，用来快速进入房间
var roomSec="";

cc.Class({
    extends: cc.Component,

    properties: {
        testBtn:cc.Node
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        let launchInfo = wx.getLaunchOptionsSync();
        cc.log("getLaunchOptionsSync");
        cc.log(launchInfo);
		if(launchInfo!=null){
			roomID=launchInfo.id;
			roomSec=launchInfo.sec;
		};
		wx.onShow((data)=>{
            cc.log("onShow");
            cc.log(data);
			if(data!=null){
				roomID=data.id;
				roomSec=data.sec;
			}
		});
	},

    start() {

        this.node.getChildByName('enterToLogin').runAction(cc.repeatForever(cc.sequence(cc.scaleTo(1.8,1.8),cc.scaleTo(1.8,0.9))));

        //盖一个用户信息授权按钮
        var sysInfo = window.wx.getSystemInfoSync();
        var width = sysInfo.screenWidth;
        var height = sysInfo.screenHeight;

        console.log("width:"+width+", height:"+height);

        let userBtn = wx.createUserInfoButton({
            type: 'text',
            text: '',
            style: {
                left: 0,
                top: 0,
                width: width,
                height: height,
                lineHeight: 40,
                backgroundColor: '#00000000',
                color: '#ffffff',
                textAlign: 'center',
                fontSize: 16,
                borderRadius: 10
            }
        });

        userBtn.onTap(function (res) {
            if (!res.userInfo) {
                console.log("获取用户信息失败");
                return 0;
            };
            //获取用户code
            wx.login({
                success: (userres) => {
                    //给服务器验证
                    globalObj.send([10001, userres.code, res.userInfo.nickName, res.userInfo.avatarUrl, roomID]);
                }
            });

            globalObj.roleHead = res.userInfo.avatarUrl;
            globalObj.roleName = res.userInfo.nickName;

            userBtn.destroy();
        });

        //方便测试
        // this.testBtn.on(cc.Node.EventType.TOUCH_END,(event)=>{
        //     let accountID = parseInt(this.node.getChildByName('editbox').getComponent(cc.EditBox).string);
        //     let nickName = "test"+accountID;
        //     let avatarUrl = "https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83erft0xkiaveqpvoq3ttSjzkhBvI6pAVmrKXQcAKI81XUl70fHjdv1E4SsbwMlgwWzmGV4nURU5mTbQ/132";
		// 	//let avatarUrl = "http://img2.touxiang.cn/file/20160423/419902dafa403dbeee4c1f723fb21424.jpg";
        //     globalObj.send([10001, accountID, nickName, avatarUrl, roomID]);
        //     globalObj.roleHead = avatarUrl;
        //     globalObj.roleName = nickName;
        // });

    },

    update (dt) {
        globalObj.checkMsgList(this);
    },

    //===============================消息处理函数=============================================
    sc_role_login(msgData){
        let result = msgData[0];
        if (result == 0){
            console.log("登录成功");
            globalObj.roleID = msgData[1];
            //切换场景
            cc.director.loadScene('roomScene');
        }else if(result==1){
            //重复登录
            globalObj.commonMsg(this.node,"重复登录，请重新连接游戏",()=>{
                cc.director.loadScene("loginScene");
                globalObj.msgList.push({msgID:2,msgData:0});
            });
        }else{
            globalObj.notice(this.node, "登录失败");
        };
    },
    netBreakDown(){
        globalObj.commonMsg(this.node,"网络断开，请重新连接游戏",()=>{
            cc.director.loadScene("loginScene");
            globalObj.msgList.push({msgID:2,msgData:0});
        });
    },
});
