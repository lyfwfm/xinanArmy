
import * as globalObj from './proto_lib';

cc.Class({
    extends: cc.Component,

    properties: {
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

        this.node.getChildByName('yesBtn').on(cc.Node.EventType.TOUCH_END,(event)=>{
            globalObj.send([10207,globalObj.nowStationID,1]);
        });
        this.node.getChildByName('noBtn').on(cc.Node.EventType.TOUCH_END,(event)=>{
            globalObj.send([10207,globalObj.nowStationID,0]);
        });
        this.node.getChildByName('closeBtn').on(cc.Node.EventType.TOUCH_END,(event)=>{
            this.node.active=false;
        });

    },

    // update (dt) {},
});
