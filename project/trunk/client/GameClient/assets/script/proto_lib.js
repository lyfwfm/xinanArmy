
//该文件作为公共文件，里面的变量和函数前面必须导出(export)，该文件内部引用则前面要加this.

export var globalws;
export function send(Msg){
    cc.log("client send msg = " + Msg);
    this.globalws.send(JSON.stringify(Msg))
}
export var roleID;
export var roleHead;
export var roleName;

export var msgList=[];

export function doCheckMsgList(thisObj,nodeName){
    for(let i=0;i<this.msgList.length;i++){
        let isDone = this.doCheckMsgList_1(thisObj,this.msgList[i].msgID,this.msgList[i].msgData);
        if(isDone){
            this.msgList[i].msgID=0;
        }else{
            console.log("no match protoData "+nodeName+" = ");
            console.log(this.msgList[i]);
        }
    };
}
//return boolean() 处理成功与否
export function doCheckMsgList_1(thisObj,msgID,msgData){
    try{
        if(msgID==10002){
            thisObj.sc_role_login(msgData);
        }else if(msgID==10004){
            thisObj.sc_role_heart_beat(msgData);
        }else if(msgID==10104){
            thisObj.sc_room_list(msgData);
        }else if(msgID==10106){
            thisObj.sc_room_enter(msgData);
        }else if(msgID==10108){
            thisObj.sc_room_create(msgData);
        }else if(msgID==10124){
            thisObj.sc_room_fast_join(msgData);
        }else if(msgID==10109){
            thisObj.sc_room_short_info(msgData);
        }else if(msgID==10113){
            thisObj.sc_room_change_pos(msgData);
        }else if(msgID==10117){
            thisObj.sc_room_kick(msgData);
        }else if(msgID==10119){
            thisObj.sc_room_leave(msgData);
        }else if(msgID==10121){
            thisObj.sc_room_change_ready(msgData);
        }else if(msgID==10122){
            thisObj.sc_room_member_list(msgData);
        }else if(msgID==10126){
            thisObj.sc_room_modify_room(msgData);
        }else if(msgID==10201){
            thisObj.sc_battle_begin(msgData);
        }else if(msgID==10202){
            thisObj.sc_battle_turn(msgData);
        }else if(msgID==10204){
            thisObj.sc_battle_uncover(msgData);
        }else if(msgID==10206){
            thisObj.sc_battle_choose(msgData);
        }else if(msgID==10208){
            thisObj.sc_battle_move(msgData);
        }else if(msgID==10209){
            thisObj.sc_battle_piece_dead(msgData);
        }else if(msgID==10210){
            thisObj.sc_battle_over(msgData);
        }else if(msgID==10212){
            thisObj.sc_battle_surrender(msgData);
        }else if(msgID==10214){
            thisObj.sc_battle_surrender_answer(msgData);
        }else if(msgID==10215){
            thisObj.sc_battle_info(msgData);
        }else if(msgID==10216){
            thisObj.sc_battle_camp(msgData);
        }else if(msgID==1){//网络连接断开
            thisObj.netBreakDown();
        }else if(msgID==2){//重连网络
            thisObj.reconnect();
        }else{
            return false;
        }
        return true;
    }
    catch (error){
        return false;
    }
}

export function checkMsgList(thisObj){
    switch(thisObj.node.name){
        case "login"://登录界面
            this.doCheckMsgList(thisObj,"login");
            break;
        case "msgNode"://msgNode常驻节点
            this.doCheckMsgList(thisObj,"msgNode");
            break;
        case "mainUI"://主界面
            this.doCheckMsgList(thisObj,"mainUI");
            break;
        case "battle"://对战界面
            this.doCheckMsgList(thisObj,"battle");
            break;
        default :
            return 0;
    };
    
    let tempMsgList=[];
    this.msgList.forEach((element)=>{
        if(element){
            if(element.msgID!=0){tempMsgList.push(element)}
        }
    });
    this.msgList=tempMsgList;
}

export function getPieceNameByID(pieceID){
    let onlyID = pieceID % 100;
    switch(onlyID){
        case 1: return "司令";
        case 2: return "军长";
        case 3:
        case 4: return "师长";
        case 5:
        case 6: return "旅长";
        case 7:
        case 8: return "团长";
        case 9:
        case 10: return "营长";
        case 11:
        case 12:
        case 13: return "连长";
        case 14:
        case 15:
        case 16: return "排长";
        case 17:
        case 18:
        case 19: return "工兵";
        case 20:
        case 21: return "炸弹";
        case 22:
        case 23:
        case 24: return "地雷";
        case 25: return "军旗";
        default: return "不知道";
    };
}
export function getPieceShortNameByID(pieceID){
    let onlyID = pieceID % 100;
    switch(onlyID){
        case 1: return "司";
        case 2: return "军";
        case 3:
        case 4: return "师";
        case 5:
        case 6: return "旅";
        case 7:
        case 8: return "团";
        case 9:
        case 10: return "营";
        case 11:
        case 12:
        case 13: return "连";
        case 14:
        case 15:
        case 16: return "排";
        case 17:
        case 18:
        case 19: return "兵";
        case 20:
        case 21: return "弹";
        case 22:
        case 23:
        case 24: return "雷";
        case 25: return "旗";
        default: return "无";
    };
}

export var uncoverPieceID=-8;

export function setHead(nodeObj,res){
	cc.loader.load({url:res,type:'png'},(err,texture)=>{
		let sfIns = new cc.SpriteFrame(texture);
		if(sfIns.textureLoaded()){
			nodeObj.getComponent(cc.Sprite).spriteFrame = sfIns;
		}else{
			cc.loader.load({url:res},(err1,texture1)=>{
				nodeObj.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(texture1);	
			});
		};
		
	});
}

//==========================飘字提示===============================
export function notice(nodeObj, noticeStr){
    let backNode = new cc.Node('noticeNode');
    let noticeLabel = backNode.addComponent(cc.Label);
    noticeLabel.string = noticeStr;
    backNode.color = cc.color(0,255,0);
    let space = nodeObj.height/4;
    backNode.y = space;
    nodeObj.addChild(backNode);
    cc.log(noticeStr);
    let callFun = cc.callFunc(this.onActionEnd,nodeObj);
    let action = cc.sequence(cc.scaleBy(0.2,2.5,2.5),cc.scaleBy(0.2,0.5,0.5),cc.spawn(cc.moveBy(1,cc.v2(0,space)),cc.fadeOut(1)),callFun);
    backNode.runAction(action);
}
export function onActionEnd(nodeObj){
    nodeObj.destroy();
}
//==========================时间相关===============================
export	var lastTimestamp = 0;
export	var serverTimestamp = 0;
export	function initTime(){
	let millseconds = this.getTickCount();
	this.lastTimestamp = millseconds;
}
export	function setServerTime(serverTime){
	this.serverTimestamp=serverTime;
	this.lastTimestamp=this.getTickCount();
}
export	function now(){
	let escaped = this.getTickCount() - this.lastTimestamp;
	return this.serverTimestamp + Math.floor(escaped/1000);
}

export function getTickCount() {
	if(window && window.performance){
		return window.performance.now();
	}
	return (new Date()).getTime();
}
//==========================时间相关 END===============================

export var fastJoinSettingList=[1,0,0,0,1,1,0];
//==========================战斗相关===============================
export var lastStationID=0;
export var nowStationID=0;
export var flagStation1=0;
export var flagStation2=0;
export var isWithFlag=0;

export var pieceInsList=[];//序号为stationID的换算 里面存储piece的实例
export var stationInsList=[];
export var stationPosList=[];
export var myPos=0;

export var roomID=0;//进入到房间才会被设置
export var roomMasterID=0;//进入到房间才会被设置
export var roomName="";//进入到房间才会被设置
export var roomSec="";//进入到房间才会被设置
export var roomSettingList=[0,0,0,0,0,0,0];//进入到房间才会被设置
export var posPlayerIDList=[];//POS对应的PLAYERID列表{pos,roleID}

export function getPosByStationID(stationID){
    return this.stationPosList[stationID%100 - 1];
}

export function getPieceInsByStationID(stationID){
    return this.pieceInsList[Math.floor(stationID%100)-1];
}

export function setPieceInsByStationID(stationID, pieceIns){
    this.pieceInsList[Math.floor(stationID%100)-1] = pieceIns;
}

export function getRoleIDByPos(pos){
    for(let i=0;i<this.posPlayerIDList.length;i++){
        if(pos==this.posPlayerIDList[i].pos){
            return this.posPlayerIDList[i].roleID;
        }
    }
    return 0
}

//停止之前棋子的动画，如果有之前棋子
export	function stopLastPieceAction(){
	let pieceIns = this.getPieceInsByStationID(this.lastStationID);
	if(pieceIns!=null){
        pieceIns.scale=1;
		pieceIns.stopAllActions();
	}
}

export function getPosStr(pos){
    if(pos==1){return "红";}
    else if(pos==2){return "蓝";}
    else{return "不知道";}
}

//==========================长按===============================
export var isTouching=false;
export var touchTime=0;
export var longTouchTime=0;//触发长按事件所需要的时间
export var callBackFun=null;//长按触发后 回调的函数
export function update(dt){
    if(this.isTouching){
        this.touchTime+=dt;
        cc.log("update touchTime="+this.touchTime);
        if(this.longTouchTime>0&&this.touchTime>=this.longTouchTime){
            //触发事件
            this.callBackFun&&this.callBackFun();
            //现在先写成只触发一次的情况
            //如果要触发多次（比如点击+号，一直加）则需要修改这里
            this.isTouching=false;
            this.touchTime=0;
            this.callBackFun=null;
            cc.log("callBackFun************");
        }
    }
}
export function onTouchStart(internalSecs, callBack){
    this.isTouching=true;
    this.touchTime=0;
    this.callBackFun=callBack;
    this.longTouchTime=internalSecs;
    cc.log("onTouchStart internalSecs="+internalSecs);
}
export function onTouchCancel(){
    this.isTouching=false;
    this.touchTime=0;
    this.callBackFun=null;
    cc.log("onTouchCancel");
}
export function onTouchEnd(){
    this.isTouching=false;
    this.touchTime=0;
    this.callBackFun=null;
    cc.log("onTouchEnd");
}

//==========================长按 END===============================

//==========================从上而下 探头的短信息弹窗===============================
export function shortMsg(nodeObj,content,confirmCallBack){
    cc.loader.loadRes("shortMsgPrefab",(err,prefab)=>{
        let shortMsgIns = cc.instantiate(prefab);
        shortMsgIns.getChildByName("txt").getComponent(cc.Label).string = content;
        shortMsgIns.getChildByName("yes_btn").on(cc.Node.EventType.TOUCH_END,(event)=>{
            confirmCallBack&&confirmCallBack();
            shortMsgIns.destroy();
        });
        shortMsgIns.getChildByName("no_btn").on(cc.Node.EventType.TOUCH_END,(event)=>{
            shortMsgIns.destroy();
        });
        //找到上边缘
        shortMsgIns.y=nodeObj.height/2;
        //找到左边应该在的位置
        shortMsgIns.x=-shortMsgIns.width/2;
        nodeObj.addChild(shortMsgIns);
        //执行动画
        let actionEndFuc = cc.callFunc((ins)=>{
            ins.destroy();
        },shortMsgIns);
        let action = cc.sequence(cc.moveBy(1.5,cc.v2(0,-shortMsgIns.height)), cc.scaleTo(5,1,1), cc.moveBy(1.5,cc.v2(0,shortMsgIns.height)), actionEndFuc);
        shortMsgIns.runAction(action);
    });
}

//==========================短信息 END===============================
//==========================通用信息弹窗===============================
export function commonMsg(nodeObj,content,confirmCallBack){
    cc.loader.loadRes("commonMsgPrefab",(err,prefab)=>{
        let commonMsgIns = cc.instantiate(prefab);
        commonMsgIns.getChildByName("txt").getComponent(cc.Label).string = content;
        commonMsgIns.getChildByName("yes_btn").on(cc.Node.EventType.TOUCH_END,(event)=>{
            confirmCallBack&&confirmCallBack();
            commonMsgIns.destroy();
        });
        commonMsgIns.getChildByName("no_btn").on(cc.Node.EventType.TOUCH_END,(event)=>{
            commonMsgIns.destroy();
        });
        nodeObj.addChild(commonMsgIns);
    });
}

//==========================通用信息弹窗 END===============================
//==========================紧贴指定控件的弹窗===============================
//默认往屏幕中心显示
export function addSideByChild(parentNode,childIns,posIns){
    //先查看相对位置控件是在上半屏幕还是下半屏幕
    if(posIns.x>=0){
        if(posIns.y>=0){
            //右上
            childIns.x = posIns.x-childIns.width/2;
            childIns.y = posIns.y - posIns.height/2-childIns.height/2;
        }else{
            //右下
            childIns.x = posIns.x-childIns.width/2;
            childIns.y = posIns.y + posIns.height/2+childIns.height/2;
        }
    }else{
        if(posIns.y>=0){
            //左上
            childIns.x = posIns.x+childIns.width/2;
            childIns.y = posIns.y - posIns.height/2-childIns.height/2;
        }else{
            //左下
            childIns.x = posIns.x+childIns.width/2;
            childIns.y = posIns.y + posIns.height/2+childIns.height/2;
        }
    }
    parentNode.addChild(childIns);
}

//==========================紧贴指定控件的弹窗 END===============================