
import * as globalObj from './proto_lib';

cc.Class({
    extends: cc.Component,

    properties: {
        stationID:0,//所在兵站ID
        pieceID:0,
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
		
		//注册长按时间
		this.node.on(cc.Node.EventType.TOUCH_START,(event)=>{
			//判断是否有军旗
			if(globalObj.lastStationID==globalObj.flagStation1!=0 || globalObj.lastStationID==globalObj.flagStation2!=0){
                globalObj.onTouchStart(2,()=>{
                    //按住2秒则确定
                    globalObj.isWithFlag=1;
                    globalObj.notice(this.node.parent,"本次移动将携带军旗");
                });
			}
			
		});
		this.node.on(cc.Node.EventType.TOUCH_CANCEL,(event)=>{
			//取消
            globalObj.isWithFlag=0;
            globalObj.onTouchCancel();
		});

        this.node.on(cc.Node.EventType.TOUCH_END,(event)=>{
            cc.log("piece clicked id="+this.stationID+" , "+this.pieceID);
            globalObj.onTouchEnd();
            if(this.pieceID==globalObj.uncoverPieceID){
                //翻棋
                globalObj.send([10203,this.stationID]);
            }else{
                if(globalObj.lastStationID==0){
                    //选中
                    globalObj.send([10205,this.stationID]);
                }else{
                    //吃棋
                    //判断是否有军旗需要移动
                    if(globalObj.lastStationID==globalObj.flagStation1 || globalObj.lastStationID==globalObj.flagStation2){
                        globalObj.nowStationID=this.stationID;
						globalObj.send([10207,this.stationID,globalObj.isWithFlag]);
						globalObj.isWithFlag=0;	
                        //cc.find("battle/isWithFlag").active=true;
                    }else{
                        //没有军旗，直接移动
                        globalObj.send([10207,this.stationID,0]);
                    }
                }
            }
        });

    },

    update (dt) {
		
	},

    //SID::stationID
    setPieceID(SID, ID){
        this.stationID = SID;
        this.pieceID = ID;
        switch(this.pieceID){
            case globalObj.uncoverPieceID://未翻开
                // todo;
                break;
            default:
                let pieceName = globalObj.getPieceNameByID(this.pieceID);
                this.node.getChildByName('label').active=true;
                this.node.getChildByName('label').getComponent(cc.Label).string = pieceName;
                let pos = Math.floor(this.pieceID/100);
                let self=this;
                if(pos == 1){
                    cc.loader.loadRes("red",(err,texture)=>{
                        self.node.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(texture);
                    });
                    // this.node.getChildByName('label').color = cc.color(255,0,0);
                }else{
                    cc.loader.loadRes("green",(err,texture)=>{
                        self.node.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(texture);
                     });
                    // this.node.getChildByName('label').color = cc.color(0,0,255);
                };
        };
    },

    //到了新的兵站
    setStation(tarStationID){

        //由外面的路径动画，代替这里直接设置位置
        // let posIns = globalObj.getPosByStationID(tarStationID);
        // this.node.x = posIns.x;
        // this.node.y = posIns.y;

        //得到原stationID的pieceIns
        let pieceIns = globalObj.getPieceInsByStationID(this.stationID);
        globalObj.setPieceInsByStationID(this.stationID,null);
        globalObj.setPieceInsByStationID(tarStationID,pieceIns);

        this.stationID = tarStationID;
    },
});
