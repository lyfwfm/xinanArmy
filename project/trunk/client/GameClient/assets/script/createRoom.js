
import * as globalObj from './proto_lib';

cc.Class({
    extends: cc.Component,

    properties: {
        createBtn:cc.Node,
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

        this.createBtn.on(cc.Node.EventType.TOUCH_END,(event)=>{
            let roomName = this.node.getChildByName('roomNameBox').getComponent(cc.EditBox).string;
            let sec = this.node.getChildByName('secBox').getComponent(cc.EditBox).string;
            let isChecked1 = this.node.getChildByName('setting1').getComponent(cc.Toggle).isChecked;
            let isChecked5 = this.node.getChildByName('setting5').getComponent(cc.Toggle).isChecked;
            let isChecked6 = this.node.getChildByName('setting6').getComponent(cc.Toggle).isChecked;
            let settingList = [isChecked1?1:0,0,0,0,isChecked5?1:0,isChecked6?1:0,0];
            let msg = [10107, roomName, sec, settingList];
            globalObj.send(msg);
        });

    },

    // update (dt) {},
});
