
import * as globalObj from './proto_lib';

cc.Class({
    extends: cc.Component,

    properties: {
        roleHeadImg:{
            default:null,
            type:cc.Node
        },
        roleNameLable:{
            default:null,
            type:cc.Label
        },
        scoreIconImg:{
            default:null,
            type:cc.Node
        },
        scoreLevelLable:{
            default:null,
            type:cc.Label
        },

        roomPrefab:cc.Prefab,
        bigRoomPrefab:cc.Prefab,
        createRoomPrefab:cc.Prefab,
        settingPrefab:cc.Prefab,
        blockPrefab:cc.Prefab,

        scrollView_roomList:cc.ScrollView,
        
        bigRoomIns:{
            default:null,
            type:cc.Node
        },
        createRoomIns:{
            default:null,
            type:cc.Node
        },
        settingIns:{
            default:null,
            type:cc.Node
        },

        


    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        

        this.roleNameLable.string = globalObj.roleName;
        this.scoreLevelLable.string = "工兵";

		//设置头像
		globalObj.setHead(this.roleHeadImg,globalObj.roleHead);

        //创建房间
        this.node.getChildByName('createBtn').on(cc.Node.EventType.TOUCH_END,(event)=>{
            //加入一层透明隔绝层
            this.addBlockIns();

            setTimeout(()=>{
                this.createRoomIns = cc.instantiate(this.createRoomPrefab);
                this.node.addChild(this.createRoomIns);
            },500);
        });

        //搜索
        this.node.getChildByName('searchBtn').on(cc.Node.EventType.TOUCH_END,(event)=>{
            //先清空列表
            this.scrollView_roomList.content.destroyAllChildren();

            let roomID = this.node.getChildByName('searchBox').getComponent(cc.EditBox).string;
            try{
                roomID = parseInt(roomID);
                if(roomID > 0){
                    globalObj.send([10101, roomID]);
                }else{
                    globalObj.send([10101, 0])
                }
            }catch(error){
                console.log(error);
            }
        });

        //快速加入
        this.node.getChildByName('joinBtn').on(cc.Node.EventType.TOUCH_END,(event)=>{
            globalObj.send([10123,globalObj.fastJoinSettingList]);
        });
        //快速加入设置
        this.node.getChildByName('settingImg').on(cc.Node.EventType.TOUCH_END,(event)=>{

            //加入一层透明隔绝层
            this.addBlockIns();

            setTimeout(()=>{
                this.settingIns = cc.instantiate(this.settingPrefab);
                this.settingIns.getComponent('settingIns').insType = 1;
                this.node.addChild(this.settingIns);
            },500);
        });

        globalObj.send([10101, 0])
    },

    update (dt) {
        //遍历消息队列
        globalObj.checkMsgList(this);
    },

    //===============================消息处理函数=============================================
    sc_room_list(roomList0) {
        let roomList = roomList0[0];
        for(let i=0;i<roomList.length;i++){
            let roomIns = cc.instantiate(this.roomPrefab);
            
            // roomIns.roomID = roomList[i][0];
            roomIns.getChildByName('roomIDTxt').getComponent(cc.Label).string = "房间号："+roomList[i][0];
            roomIns.getChildByName('roomNameTxt').getComponent(cc.Label).string = roomList[i][1];
            
            if(roomList[i][2]==0){
                roomIns.getChildByName('lockImg').active=false;
            }else{
                roomIns.getChildByName('lockImg').active=true;
                cc.loader.loadRes('lock.png',(err,texture)=>{
                    roomIns.getChildByName('lockImg').getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(texture);
                });
            }
            roomIns.getChildByName('watchingNumTxt').getComponent(cc.Label).string = roomList[i][3]+"人观战";

            this.setPlayerInfo(roomIns.getChildByName('roleName1'),roomIns.getChildByName('role1Img'),roomList[i][5]);
            this.setPlayerInfo(roomIns.getChildByName('roleName2'),roomIns.getChildByName('role2Img'),roomList[i][6]);

            roomIns.on(cc.Node.EventType.TOUCH_END,(event)=>{
                setTimeout(()=>{
                    //加入一层透明隔绝层
                    this.addBlockIns();

                    this.bigRoomIns = cc.instantiate(this.bigRoomPrefab);

                    this.bigRoomIns.getComponent('bigRoom').roomID = roomList[i][0];
                    this.bigRoomIns.getComponent('bigRoom').isSec=roomList[i][2];
                    this.bigRoomIns.getComponent('bigRoom').roomSettingList = roomList[i][4];
                    this.bigRoomIns.getChildByName('roomNameTxt').getComponent(cc.Label).string = roomList[i][1];
                    
                    if(roomList[i][2]==0){
                        this.bigRoomIns.getChildByName('lockImg').active=false;
                    }else{
                        this.bigRoomIns.getChildByName('lockImg').active=true;
                        cc.loader.loadRes('lock.png',(err,texture)=>{
                            this.bigRoomIns.getChildByName('lockImg').getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(texture);
                        });
                    }
                    this.bigRoomIns.getChildByName('watchingNumTxt').getComponent(cc.Label).string = roomList[i][3]+"人观战";

                    this.setPlayerInfo(this.bigRoomIns.getChildByName('roleName1'),this.bigRoomIns.getChildByName('role1Img'),roomList[i][5]);
                    this.setPlayerInfo(this.bigRoomIns.getChildByName('roleName2'),this.bigRoomIns.getChildByName('role2Img'),roomList[i][6]);

                    this.node.addChild(this.bigRoomIns)
                },500);
            });
            
            this.scrollView_roomList.content.addChild(roomIns);
        };

        //滑动块置顶
        this.scrollView_roomList.scrollToOffset(cc.v2(0,0));

        if(roomList.length<=0){
            globalObj.notice(this.node,"还没有房间，建立一个吧");
        }
        
    },

    sc_room_create(msgData){
        if(msgData[0] != 0){
            globalObj.notice(this.node,"创建房间失败："+msgData[0]);
            cc.log("sc_room_create result = "+msgData[0]);
        }else{
            cc.director.loadScene('battleScene');
        }
    },

    sc_room_enter(msgData){
        if(msgData[0] != 0){
            globalObj.notice(this.node,"进入房间失败："+msgData[0]);
            cc.log("sc_room_enter result = "+msgData[0]);
        }else{
            cc.director.loadScene('battleScene');
        }
    },

    sc_room_fast_join(msgData){
        if(msgData[0]==0){
            //成功会走到上面sc_room_enter
        }else{
            globalObj.notice(this.node,"按照快速加入的规则，还没有可加入的房间");
        }
    },

    netBreakDown(){
        globalObj.commonMsg(this.node,"网络断开，请重新连接游戏",()=>{
            cc.director.loadScene("loginScene");
            globalObj.msgList.push({msgID:2,msgData:0});
        });
    },

    sc_role_login(msgData){
        let result = msgData[0];
        if (result == 0){
            console.log("登录成功");
            globalObj.roleID = msgData[1];
            //切换场景
            cc.director.loadScene('roomScene');
        }else if(result==1){
            //重复登录
            globalObj.commonMsg(this.node,"重复登录，请重新连接游戏",()=>{
                cc.director.loadScene("loginScene");
                globalObj.msgList.push({msgID:2,msgData:0});
            });
        }else{
            globalObj.notice(this.node, "登录失败");
        };
    },

    

    //=======================================LOCAL FUNCTION===================================================
    //设置界面上对战双方信息
    setPlayerInfo(roleNameNode,roleImgNode,playerData){
        if(playerData[2]==""){
            roleNameNode.getComponent(cc.Label).string = "空位";
        }else{
            roleNameNode.getComponent(cc.Label).string = playerData[2];
        };

        if(playerData[3]!=""){
			globalObj.setHead(roleImgNode,playerData[3]);
        }
    },

    //加入一层透明隔绝层
    addBlockIns(){
        let blockIns = cc.instantiate(this.blockPrefab);
        blockIns.width = this.node.getChildByName('roomBackground').width;
        blockIns.height = this.node.getChildByName('roomBackground').height;
        blockIns.x=0;blockIns.y=0;
        blockIns.on(cc.Node.EventType.TOUCH_END,(event)=>{
            if(this.bigRoomIns){
                this.bigRoomIns.destroy();
                this.bigRoomIns = null;
            };
            if(this.createRoomIns){
                this.createRoomIns.destroy();
                this.createRoomIns = null;
            };
            if(this.settingIns){
                this.settingIns.destroy();
                this.settingIns = null;
            };
        });
        this.node.addChild(blockIns);
    },

    
    
});