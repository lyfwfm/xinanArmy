
import * as globalObj from './proto_lib';

cc.Class({
    extends: cc.Component,

    properties: {
        stationID:0,
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        this.node.on(cc.Node.EventType.TOUCH_END,(event)=>{
            if(globalObj.lastStationID!=0){
                //移动
                //判断是否有军旗需要移动
                if(globalObj.lastStationID==globalObj.flagStation1 || globalObj.lastStationID==globalObj.flagStation2){
                    globalObj.nowStationID=this.stationID;
                    globalObj.send([10207,this.stationID,globalObj.isWithFlag]);
                    // cc.find("battle/isWithFlag").active=true;
                }else{
                    //没有军旗，直接移动
                    globalObj.send([10207,this.stationID,0]);
                }
            }
        });

    },

    // update (dt) {},

    setStationID(ID){
        this.stationID=ID;
    },
});
