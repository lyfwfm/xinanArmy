
import * as globalObj from './proto_lib';

var pos1=1;
var pos2=2;

var turnEndTimestamp=0;
var currentPos=0;
var isShakeing=false;//是否正在抖动，如果正在抖动，则不再检测

var battleMenuIns=null;
var headMenuIns=null;

cc.Class({
    
    extends: cc.Component,

    properties: {
        piecePrefab:cc.Prefab,
        statioinPrefab:cc.Prefab,// 主要用来响应点击事件
        deadPiece:cc.Prefab,
        deadX:cc.Prefab,
        blockPrefab:cc.Prefab,
        settingPrefab:cc.Prefab,
        battleMenuPrefab:cc.Prefab,
        headMenuPrefab:cc.Prefab,

        settingIns:{
            default:null,
            type:cc.Node
        },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        
    },

    start () {
        //初始化棋盘兵站的位置信息
        this.initStationList();

        this.node.getChildByName('readyBtn').on(cc.Node.EventType.TOUCH_END,(event)=>{
            globalObj.send([10120]);
        });

        this.node.getChildByName('isWithFlag').active=false;

        //房间设置按钮
        this.node.getChildByName('settingImg').on(cc.Node.EventType.TOUCH_END,(event)=>{

            //加入一层透明隔绝层
            this.addBlockIns(1);

            setTimeout(()=>{
                this.openSettingIns();
            },500);
        });

        //给空位注册点击事件
        this.node.getChildByName('empty1').on(cc.Node.EventType.TOUCH_END,(event)=>{
            globalObj.send([10112, pos1]);
        });
        this.node.getChildByName('empty2').on(cc.Node.EventType.TOUCH_END,(event)=>{
            globalObj.send([10112, pos2]);
        });

        //todo 测试分享
        // this.node.getChildByName('scoreIcon1').on(cc.Node.EventType.TOUCH_END,(event)=>{
        //     this.clickShare();
        // });

    },

    update (dt) {
        //遍历消息队列
        globalObj.checkMsgList(this);
		
		//检测是否沙漏抖动 15秒开始抖动
		let now = globalObj.now();
		if(!isShakeing && turnEndTimestamp>0 && (turnEndTimestamp-now)<15){
			isShakeing=true;
			//开始抖动
			switch(currentPos){
                case 1:
					this.node.getChildByName('clock'+pos1).runAction(cc.repeatForever(cc.sequence(cc.scaleTo(0.5,1.5),cc.scaleTo(0.5,0.5))));
					break;
				case 2:
					this.node.getChildByName('clock'+pos2).runAction(cc.repeatForever(cc.sequence(cc.scaleTo(0.5,1.5),cc.scaleTo(0.5,0.5))));
					break;
				default:
					
			}
		}
    },


    //===============================消息处理函数=============================================
    sc_room_short_info(msgData){
        this.setMemberList(msgData[0],true,msgData[5]);
        //自己缓存 房主、房间名称、密码、规则
        globalObj.roomMasterID = msgData[1];
        globalObj.roomSettingList=msgData[2];
        globalObj.roomName=msgData[3];
        globalObj.roomSec=msgData[4];
		globalObj.roomID=msgData[6];
    },

    sc_battle_info(msgData){
        this.setStationList(msgData[0]);
        this.setFlag(msgData[1],1);
        this.setFlag(msgData[2],2);
        this.setDeadList(msgData[3]);
        this.setTurn(msgData[4],msgData[5]);
    },

    sc_room_change_ready(msgData){
        switch(msgData[0]){
            case 0:
                if(msgData[2]==1){
                    globalObj.notice(this.node,"取消准备");
                }else{
                    globalObj.notice(this.node,"准备战斗");
                }
                break;
            case 1:
                globalObj.notice(this.node,"你不是战斗人员");        
                break;
            case 2:
                globalObj.notice(this.node,"已经开始战斗了，无法修改");
                break;
            default:
        }
    },

    sc_battle_begin(msgData){
        this.node.getChildByName('readyBtn').active = false;
        globalObj.lastStationID=0;
        globalObj.notice(this.node,"战斗开始");
    },

    sc_battle_turn(msgData){
		//停止之前棋子的动画
		globalObj.stopLastPieceAction();
        this.setTurn(msgData[0],msgData[1]);
    },

    sc_battle_uncover(msgData){
		//停止之前棋子的动画
		globalObj.stopLastPieceAction();
        if(msgData[0]!=0){
            switch(msgData[0]){
                case 1:
                    globalObj.notice(this.node,"还没开始战斗");
                    break;
                case 2:
                    globalObj.notice(this.node,"你不是战斗人员");        
                    break;
                case 3:
                    globalObj.notice(this.node,"对方回合");        
                    break;
                default:
            }
        }else{
            let pieceIns = globalObj.getPieceInsByStationID(msgData[2]);
            pieceIns.getComponent('piece').setPieceID(msgData[2],msgData[3]);
        };
        globalObj.lastStationID=0;

    },

    sc_battle_choose(msgData){
		//停止之前棋子的动画
		globalObj.stopLastPieceAction();
        if(msgData[0]==0){
            globalObj.lastStationID=msgData[2];
			//被选中棋子做动画
			let pieceIns = globalObj.getPieceInsByStationID(msgData[2]);
			if(pieceIns != null){
                pieceIns.runAction(cc.repeatForever(cc.sequence(cc.scaleTo(0.5,1.5),cc.scaleTo(0.5,1))));
			}
        }else{
            globalObj.lastStationID=0;
            globalObj.notice(this.node,"取消选中");
        }
    },

    sc_battle_move(msgData){
		//停止之前棋子的动画
		globalObj.stopLastPieceAction();
        if(msgData[0]!=0){
            switch(msgData[0]){
                case 1:
                    globalObj.notice(this.node,"还没开始战斗");
                    break;
                case 2:
                    globalObj.notice(this.node,"你不是战斗人员");        
                    break;
                case 3:
                    globalObj.notice(this.node,"对方回合");        
                    break;
                case 4:
                    switch(msgData[1]){
                        case 3:
                            globalObj.notice(this.node,"无法移动过去");
                            break;
                        case 5:
                            globalObj.notice(this.node,"规则限制，无法移动过去");
                            break;
                        case 8:
                            globalObj.notice(this.node,"打不过，无法移动过去");
                            break;
                        default:
                    }
                    break;
                default:
            }
        }else{
            //成功移动
            let srcStationID = msgData[3];
            let tarStationID = msgData[4];
            let srcPieceIns = globalObj.getPieceInsByStationID(srcStationID);
            let tarPieceIns = globalObj.getPieceInsByStationID(tarStationID);

            //执行移动动画
            this.pieceMoveAction(srcPieceIns,msgData[5],tarStationID);

            switch(msgData[1]){
                case 0://空地直接移动
                    srcPieceIns.getComponent('piece').setStation(tarStationID);
                    break;
                case 1://吃掉对方
                    if(tarPieceIns){tarPieceIns.destroy();}
                    srcPieceIns.getComponent('piece').setStation(tarStationID);
                    break;
                case 2://双方对掉
                    if(srcPieceIns){srcPieceIns.destroy();}
                    if(tarPieceIns){tarPieceIns.destroy();}
                    break;
                case 6://上军旗
                    srcPieceIns.getComponent('piece').setStation(tarStationID);
                    break;
                default:
                    //todo
                    cc.log("***********unknow moveResult:"+msgData[1]);
            }
        };
        globalObj.lastStationID=0;
    },

    sc_battle_piece_dead(msgData){
        this.setDeadList(msgData[0]);
    },

    sc_battle_over(msgData){
		//停止之前棋子的动画
		globalObj.stopLastPieceAction();
        //todo 弹窗显示胜利失败
        globalObj.notice(this.node,"游戏结束！！！");
        if(globalObj.myPos<=2){
            this.node.getChildByName('readyBtn').active = true;
        };
        globalObj.lastStationID=0;
        //沙漏抖动相关
		isShakeing=false;
		this.node.getChildByName('clock1').active=false;
		this.node.getChildByName('clock2').active=false;
    },

    sc_battle_camp(msgData){
        if(globalObj.roleID == msgData[0]){
            //pos=1
            this.node.getChildByName('roleName2').color = cc.color(255,0,0);
            this.node.getChildByName('roleName1').color = cc.color(0,0,255);
            //重新更正阵营
            globalObj.myPos = 1;
        }else if(globalObj.roleID == msgData[1]){
            //pos=2
            this.node.getChildByName('roleName1').color = cc.color(255,0,0);
            this.node.getChildByName('roleName2').color = cc.color(0,0,255);
            //重新更正阵营
            globalObj.myPos = 2;
        }else{
            //观战者
            this.node.getChildByName('roleName1').color = cc.color(255,0,0);
            this.node.getChildByName('roleName2').color = cc.color(0,0,255);
        }
    },

    sc_room_member_list(msgData){
        this.setMemberList(msgData[0],false,msgData[1]);
    },

    sc_room_modify_room(msgData){
        //自己是否是房主
        if(globalObj.roleID==globalObj.roomMasterID){
            //房主
            if(msgData[0]==0){
                globalObj.roomName=msgData[1];
                globalObj.roomSec=msgData[2];
                globalObj.roomSettingList=msgData[3];
                globalObj.notice(this.node,"修改成功");
            }else{
                globalObj.notice(this.node,"修改失败");
            }
        }else{
            //房友
            if(msgData[0]==0){
                globalObj.roomName=msgData[1];
                globalObj.roomSec=msgData[2];
                globalObj.roomSettingList=msgData[3];
                globalObj.notice(this.node,"房主修改了房间信息");
            }
        }
    },

    sc_room_change_pos(msgData){
        if(msgData[0]==0){
            //成功更换位置
            let srcPos = msgData[2];
            let tarPos = msgData[3];
            //globalObj.myPos和pos1 pos2在memberList函数里面重置
            if(srcPos>2){
                //非战斗位置todo
            }else{
                //战斗位置
                //重置为空位
                this.setActiveByPos(srcPos,false);
            }
            if(tarPos>2){
                //非战斗位置
            }else{
                //战斗位置
                this.setActiveByPos(tarPos,true);
            }
        }else{
            return 0;
        }
    },

    sc_room_leave(msgData){
        if(msgData[0]==0){
            if(globalObj.roleID == msgData[1]){
                //自己离开房间
                //切换场景
                cc.director.loadScene('roomScene');
            }else{
                //其他人离开房间
                //将位置复原
                this.setActiveByPos(msgData[2],false);
            }
        }else{
            globalObj.notice(this.node,"不能离开房间:"+msgData[0])
        }
    },

    sc_battle_surrender(msgData){
        if(msgData[0]==0){
            switch(msgData[3]){
                case 1://投降
                    globalObj.notice(this.node,globalObj.getPosStr(msgData[2])+"方投降认输");
                    break;
                case 2://求和
                    globalObj.notice(this.node,globalObj.getPosStr(msgData[2])+"方请求平局");
                    if(globalObj.myPos<=2){
                        //我是战斗人员
                        if(globalObj.myPos!=msgData[2]){
                            globalObj.shortMsg(this.node,"请求平局",()=>{
                                globalObj.send([10213,1]);
                            });
                        }
                    }
                    break;
                default:
            }
        }else{

        }        
    },
    sc_battle_surrender_answer(msgData){},

    sc_room_kick(msgData){
        if(msgData[0]==0){
            if(globalObj.roleID == msgData[1]){
                //自己被移出房间
                globalObj.notice(this.node,"被房主移出房间");
                //切换场景
                cc.director.loadScene('roomScene');
            }else{
                //其他人被移出房间
                //将位置复原
                this.setActiveByPos(msgData[2],false);
            }
        }
    },

    netBreakDown(){
        globalObj.commonMsg(this.node,"网络断开，请重新连接游戏",()=>{
            cc.director.loadScene("loginScene");
            globalObj.msgList.push({msgID:2,msgData:0});
        });
    },
    sc_role_login(msgData){
        let result = msgData[0];
        if (result == 0){
            console.log("登录成功");
            globalObj.roleID = msgData[1];
            //切换场景
            cc.director.loadScene('roomScene');
        }else if(result==1){
            //重复登录
            globalObj.commonMsg(this.node,"重复登录，请重新连接游戏",()=>{
                cc.director.loadScene("loginScene");
                globalObj.msgList.push({msgID:2,msgData:0});
            });
        }else{
            globalObj.notice(this.node, "登录失败");
        };
    },


    //=======================================LOCAL FUNCTION===================================================
    setMemberList(memberList,isInit,roomState){
        let isFighter = false;
        globalObj.posPlayerIDList=[];
        memberList.forEach((element)=>{
            globalObj.posPlayerIDList.push({pos:element[0],roleID:element[1]});
            if(globalObj.roleID==element[1]){
                switch(element[0]){
                    case 1:
                        isFighter=true;
                        //重置pos存储的真实位置
                        pos1=2;
                        pos2=1;
                        break;
                    case 2:
                        isFighter=true;
                        //重置pos存储的真实位置
                        pos1=1;
                        pos2=2;
                        break;
                    default:
                        //todo
                        //重置pos存储的真实位置
                        pos1=1;
                        pos2=2;
                }
            }
        });
        for(let i=0;i<memberList.length;i++){
            let memberPos = memberList[i][0];
            this.setActiveByPos(memberPos,true);
            if(globalObj.roleID==memberList[i][1]){
                if(memberPos>2){
                    //自己是观战者
                    this.node.getChildByName('readyBtn').active = false;
                }else{
                    //自己是参战者
					globalObj.setHead(this.node.getChildByName('roleImg2'),memberList[i][3]);
                    this.node.getChildByName('roleName2').getComponent(cc.Label).string = memberList[i][2];
                    if(roomState==1){this.node.getChildByName('readyBtn').active = true;}//休闲状态才设置可用
                    if(
                        ((globalObj.myPos==0 && memberPos==1)
                        ||(globalObj.myPos!=0&&globalObj.myPos!=memberPos))
                            && isInit){
                                globalObj.stationPosList.reverse();
                            }

                };
                globalObj.myPos=memberPos;
            }else{
                //别人
                switch(memberPos){
                    case 1:
						globalObj.setHead(this.node.getChildByName('roleImg1'),memberList[i][3]);
                        this.node.getChildByName('roleName1').getComponent(cc.Label).string = memberList[i][2];
                        break;
                    case 2://默认以2号视角
                        if(isFighter){
							globalObj.setHead(this.node.getChildByName('roleImg1'),memberList[i][3]);
                            this.node.getChildByName('roleName1').getComponent(cc.Label).string = memberList[i][2];
                        }else{
							globalObj.setHead(this.node.getChildByName('roleImg2'),memberList[i][3]);
                            this.node.getChildByName('roleName2').getComponent(cc.Label).string = memberList[i][2];
                        }
                        break;
                    default:
                        // todo;
                }
            }
        };
    },

    setStationList(pieceIDList){
        //先清空之前的实例-兵站
        for(let i=0;i<globalObj.stationInsList.length;i++){
            if(globalObj.stationInsList[i]){
                globalObj.stationInsList[i].destroy();
            }
        };
        globalObj.stationInsList=[];

        for(let i=0;i<globalObj.stationPosList.length;i++){
            let stationIns = cc.instantiate(this.statioinPrefab);
            stationIns.getComponent('station').setStationID(100+i+1);
            stationIns.x = globalObj.stationPosList[i].x;
            stationIns.y = globalObj.stationPosList[i].y;
            this.node.getChildByName('cheseBG').addChild(stationIns);
            globalObj.stationInsList.push(stationIns);
        };

        //先清空之前的实例-棋子
        for(let i=0;i<globalObj.pieceInsList.length;i++){
            if(globalObj.pieceInsList[i]){
                globalObj.pieceInsList[i].destroy();
            }
        };
        globalObj.pieceInsList=[];

        for(let i=0;i<pieceIDList.length;i++){
            if(pieceIDList[i]!=0){
                let pieceIns = cc.instantiate(this.piecePrefab);
                pieceIns.getComponent('piece').setPieceID(100+i+1,pieceIDList[i]);
                
                pieceIns.x = globalObj.stationPosList[i].x;
                pieceIns.y = globalObj.stationPosList[i].y;
                this.node.getChildByName('cheseBG').addChild(pieceIns);   
                globalObj.pieceInsList.push(pieceIns);
            }else{
                globalObj.pieceInsList.push(null);
            }
        };
    },

    setFlag(stationID,pos){
        if(pos==1){
            globalObj.flagStation1=stationID;
        }else if(pos==2){
            globalObj.flagStation2=stationID;
        }

    },

    //设置死亡池
    setDeadList(deadList){
        //先清空
        this.node.getChildByName('deadPool1_1').destroyAllChildren();
        this.node.getChildByName('deadPool1_2').destroyAllChildren();
        this.node.getChildByName('deadPool2_1').destroyAllChildren();
        this.node.getChildByName('deadPool2_2').destroyAllChildren();
        //先分阵营
        let deadList1 = [];
        let deadList2 = [];
        deadList.forEach((pieceID)=>{
            if(Math.floor(pieceID/100)==1){
                let found = deadList1.find((element)=> {return element.id==pieceID;});
                if(found){
                    found.count += 1;
                }else{
                    deadList1.push({id:pieceID,count:1});
                }
            }else{
                let found = deadList2.find((element)=> {return element.id==pieceID;});
                if(found){
                    found.count += 1;
                }else{
                    deadList2.push({id:pieceID,count:1});
                }
            }
        });

        //设置控件
        deadList1.forEach((element)=>{
            switch(globalObj.myPos){
                case 1:
                    //设置到他方
                    this.doSetDeadList(element, 'deadPool1_1','deadPool1_2');
                    break;
                case 2:
                    this.doSetDeadList(element, 'deadPool2_1','deadPool2_2');
                    break;
                default:
                    this.doSetDeadList(element, 'deadPool2_1','deadPool2_2');
            }
        });
        deadList2.forEach((element)=>{
            switch(globalObj.myPos){
                case 1:
                    this.doSetDeadList(element, 'deadPool2_1','deadPool2_2');
                    break;
                case 2:
                    this.doSetDeadList(element, 'deadPool1_1','deadPool1_2');
                    break;
                default:
                    this.doSetDeadList(element, 'deadPool1_1','deadPool1_2');
            }
        });

    },

    setTurn(turnPos,turnEndTime){
        globalObj.lastStationID=0;
        if(turnPos==1){
            globalObj.notice(this.node,"红方回合");
        }else if(turnPos==2){
            globalObj.notice(this.node,"蓝方回合");
        }
        switch(turnPos){
            case 1:
                this.node.getChildByName('clock'+pos1).active=true;
                this.node.getChildByName('clock'+pos2).active=false;
                break;
            case 2:
                this.node.getChildByName('clock'+pos2).active=true;
                this.node.getChildByName('clock'+pos1).active=false;
                break;
        };
		//沙漏抖动相关
		turnEndTimestamp = turnEndTime;
		currentPos = turnPos;
		isShakeing=false;
		this.node.getChildByName('clock1').stopAllActions();
		this.node.getChildByName('clock2').stopAllActions();
		
    },

    initStationList(){
        //每个兵站相对于中心（第二个前线）的 X,Y格子数
        let stationGridList=[
            {x:-2,y:703},{x:-1,y:703},{x:0,y:703},{x:1,y:703},{x:2,y:703},
            {x:-2,y:595},{x:-1,y:595},{x:0,y:595},{x:1,y:595},{x:2,y:595},
            {x:-2,y:492},{x:-1,y:492},{x:0,y:492},{x:1,y:492},{x:2,y:492},
            {x:-2,y:382},{x:-1,y:382},{x:0,y:382},{x:1,y:382},{x:2,y:382},
            {x:-2,y:272},{x:-1,y:272},{x:0,y:272},{x:1,y:272},{x:2,y:272},
            {x:-2,y:162},{x:-1,y:162},{x:0,y:162},{x:1,y:162},{x:2,y:162},
            {x:-2,y:0},{x:-1,y:0},{x:0,y:0},{x:1,y:0},{x:2,y:0},
            {x:-2,y:-162},{x:-1,y:-162},{x:0,y:-162},{x:1,y:-162},{x:2,y:-162},
            {x:-2,y:-272},{x:-1,y:-272},{x:0,y:-272},{x:1,y:-272},{x:2,y:-272},
            {x:-2,y:-382},{x:-1,y:-382},{x:0,y:-382},{x:1,y:-382},{x:2,y:-382},
            {x:-2,y:-492},{x:-1,y:-492},{x:0,y:-492},{x:1,y:-492},{x:2,y:-492},
            {x:-2,y:-595},{x:-1,y:-595},{x:0,y:-595},{x:1,y:-595},{x:2,y:-595},
            {x:-2,y:-703},{x:-1,y:-703},{x:0,y:-703},{x:1,y:-703},{x:2,y:-703}
        ];

        
        let spaceX = 217;//横向的兵站间隔
        let spaceY = 1;//纵向的兵站间隔
        for(let i=0;i<stationGridList.length;i++){
            let tempX = stationGridList[i].x*spaceX;
            let tempY = stationGridList[i].y*spaceY;
            globalObj.stationPosList.push({x:tempX,y:tempY});
        };
    },

    doSetDeadList(element, deadName1,deadName2){
        let deadPieceIns = cc.instantiate(this.deadPiece);
        // cc.loader.loadRes('dead_junzhang',(err,texture)=>{
        //     deadPieceIns.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(texture);
        // });
        deadPieceIns.getChildByName("label").getComponent(cc.Label).string = globalObj.getPieceShortNameByID(element.id);
        this.node.getChildByName(deadName1).addChild(deadPieceIns);
        for(let i=0;i<element.count;i++){
            let deadXIns = cc.instantiate(this.deadX);
            cc.loader.loadRes('dead_x',(err,texture)=>{
                deadXIns.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(texture);
            });
            this.node.getChildByName(deadName2).addChild(deadXIns);
        };
        //填充
        for(let i=0;i<3-element.count;i++){
            let deadXIns = cc.instantiate(this.deadX);
            cc.loader.loadRes('deadX_none',(err,texture)=>{
                deadXIns.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(texture);
            });
            this.node.getChildByName(deadName2).addChild(deadXIns);
        };
    },

    //加入一层透明隔绝层
    addBlockIns(level){
        let blockIns = cc.instantiate(this.blockPrefab);
        blockIns.width = this.node.getChildByName('battleBG').width;
        blockIns.height = this.node.getChildByName('battleBG').height;
        blockIns.x=0;blockIns.y=0;
        blockIns.on(cc.Node.EventType.TOUCH_END,(event)=>{
            if(level==2&&this.settingIns){
                this.settingIns.destroy();
                this.settingIns = null;
            };
            if(level==1&&battleMenuIns){
                battleMenuIns.destroy();
                battleMenuIns = null;
            };
            if(headMenuIns){
                headMenuIns.destroy();
                headMenuIns = null;
            };
        });
        this.node.addChild(blockIns);
    },

    pieceMoveAction(pieceIns,idList,targetStationID){
        idList.push(targetStationID);
        let moveActions=[];
		idList.forEach((stationID)=>{
            let pos = globalObj.getPosByStationID(stationID);
            let action = cc.moveTo(0.2,cc.v2(pos.x,pos.y));
            moveActions.push(action);
        });
        if(moveActions.length>1){
            pieceIns.runAction(cc.sequence(moveActions));
        }else{
            pieceIns.runAction(moveActions[0]);
        }
    },
	
	clickShare(){
		wx.shareAppMessage({
				title:"快来与我一战",
				imageUrl:'sharePic',
				query:"id="+globalObj.roomID
			});
    },
    
    openSettingIns(){
        battleMenuIns = cc.instantiate(this.battleMenuPrefab);
        battleMenuIns.getChildByName('battle_btn').on(cc.Node.EventType.TOUCH_END,(event)=>{
            this.clickShare();
            battleMenuIns.destroy();
        });
        battleMenuIns.getChildByName('setting_btn').on(cc.Node.EventType.TOUCH_END,(event)=>{
            this.addBlockIns(2);
            this.settingIns = cc.instantiate(this.settingPrefab);
            this.settingIns.getComponent('settingIns').insType = 3;
            this.settingIns.getComponent('settingIns').roomID = globalObj.roomID;
            this.node.addChild(this.settingIns);
        });
        battleMenuIns.getChildByName('giveup_btn').on(cc.Node.EventType.TOUCH_END,(event)=>{
            globalObj.send([10211,1]);
            globalObj.notice(this.node,"投降");
            battleMenuIns.destroy();
        });
        battleMenuIns.getChildByName('peace_btn').on(cc.Node.EventType.TOUCH_END,(event)=>{
            globalObj.send([10211,2]);
            globalObj.notice(this.node,"求和");
            battleMenuIns.destroy();
        });
        battleMenuIns.getChildByName('leave_btn').on(cc.Node.EventType.TOUCH_END,(event)=>{
            globalObj.send([10118]);
            battleMenuIns.destroy();
        });
        //设置位置在按钮上面
        // this.node.addChild(battleMenuIns);
        globalObj.addSideByChild(this.node,battleMenuIns,this.node.getChildByName('settingImg'));
    },

    //通过POS来设置头像相关组件是否可用
    setActiveByPos(pos,isActive){
        let finalpos=0;
        if(pos==1){finalpos=pos1;}
        else if(pos==2){finalpos=pos2;}
        else {return finalpos;}

        this.node.getChildByName('empty'+finalpos).active=!isActive;
        this.node.getChildByName('roleImg'+finalpos).active=isActive;
        this.node.getChildByName('roleName'+finalpos).active=isActive;
        this.node.getChildByName('scoreIcon'+finalpos).active=isActive;
        if(isActive){
            //如果是激活，则注册点击事件
            this.node.getChildByName('roleImg'+finalpos).on(cc.Node.EventType.TOUCH_END,(event)=>{
                //自己是否是房主
                if(globalObj.roleID==globalObj.roomMasterID){
                    let roleID = globalObj.getRoleIDByPos(pos);
                    if(roleID>0){
                        //弹窗
                        this.addBlockIns(3);
                        headMenuIns = cc.instantiate(this.headMenuPrefab);
                        headMenuIns.getChildByName('kick_btn').on(cc.Node.EventType.TOUCH_END,(event)=>{
                            globalObj.send([10116,roleID]);
                            headMenuIns.destroy();
                        });
                        globalObj.addSideByChild(this.node,headMenuIns,this.node.getChildByName('roleImg'+finalpos));
                    }
                }
            });
        }
    },

});
