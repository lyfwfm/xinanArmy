
import * as globalObj from './proto_lib';

cc.Class({
    extends: cc.Component,

    properties: {
        insType:0,//通过类型来判断显示。1-快速加入的设置，2-查看房间的设置，3-战斗房间内的设置
        roomID:0,
        roomName:"",
        roomSettingList:[],
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {
    // },

    start () {
        switch(this.insType){
            case 1:
                this.onInsType1();
                break;
            case 2:
                this.onInsType2();
                break;
            case 3:
                this.onInsType3();
                break;
            default:
                console.log("error insType="+this.insType);
        }
    },

    // update (dt) {},

    //快速加入的设置
    onInsType1(){
        this.node.getChildByName('label1').active=false;
        this.node.getChildByName('roomNameBox').active=false;
        this.node.getChildByName('label2').active=false;
        this.node.getChildByName('secBox').active=false;
        this.node.getChildByName('roomIDTxt').active=false;
        this.node.getChildByName('setting1').getComponent(cc.Toggle).isChecked = globalObj.fastJoinSettingList[0]==1;
        this.node.getChildByName('setting5').getComponent(cc.Toggle).isChecked = globalObj.fastJoinSettingList[4]==1;
        this.node.getChildByName('setting6').getComponent(cc.Toggle).isChecked = globalObj.fastJoinSettingList[5]==1;
        this.node.getChildByName('confirmBtn').on(cc.Node.EventType.TOUCH_END,(event)=>{
            let isChecked1 = this.node.getChildByName('setting1').getComponent(cc.Toggle).isChecked;
            let isChecked5 = this.node.getChildByName('setting5').getComponent(cc.Toggle).isChecked;
            let isChecked6 = this.node.getChildByName('setting6').getComponent(cc.Toggle).isChecked;
            let settingList = [isChecked1?1:0,0,0,0,isChecked5?1:0,isChecked6?1:0,0];
            globalObj.fastJoinSettingList = settingList;
            this.node.destroy();
        });
    },
    //查看房间的设置
    onInsType2(){
        //房间名称
        let roomNameNode = new cc.Node('roomNameNode');
        let roomNameLabel = roomNameNode.addComponent(cc.Label);
        roomNameLabel.string=this.roomName;
        roomNameNode.x = this.node.getChildByName('roomNameBox').x;
        roomNameNode.y = this.node.getChildByName('roomNameBox').y;
        roomNameNode.width = this.node.getChildByName('roomNameBox').width/2;
        roomNameNode.height = this.node.getChildByName('roomNameBox').height/2;
        roomNameNode.scale = 0.5;
        this.node.addChild(roomNameNode);
        this.node.getChildByName('roomNameBox').active=false;
        this.node.getChildByName('roomIDTxt').getComponent(cc.Label).string="房间号："+this.roomID;
        //密码
        this.node.getChildByName('label2').active=false;
        this.node.getChildByName('secBox').active=false;
        //规则
        let settingList = [1,5,6];
        for(let i=0;i<settingList.length;i++){
            let index = settingList[i];
            let settingNode = new cc.Node('settingNode');
            let settingLabel = settingNode.addComponent(cc.Label);
            if(this.roomSettingList[index-1]==1){
                settingLabel.string="●";
            }else{
                settingLabel.string="○";
            }
            settingNode.x = this.node.getChildByName('setting'+index).x;
            settingNode.y = this.node.getChildByName('setting'+index).y;
            settingNode.width = this.node.getChildByName('setting'+index).width;
            settingNode.height = this.node.getChildByName('setting'+index).height;
            settingNode.scale = 0.5;
            this.node.addChild(settingNode);
            this.node.getChildByName('setting'+index).active=false;
        };
        this.node.getChildByName('confirmBtn').on(cc.Node.EventType.TOUCH_END,(event)=>{
            this.node.destroy();
        });
    },
    //战斗房间内的设置
    onInsType3(){
        if(globalObj.roleID!=globalObj.roomMasterID){
            //房友
            //房间名称
            let roomNameNode = new cc.Node('roomNameNode');
            let roomNameLabel = roomNameNode.addComponent(cc.Label);
            roomNameLabel.string=globalObj.roomName;
            roomNameNode.x = this.node.getChildByName('roomNameBox').x;
            roomNameNode.y = this.node.getChildByName('roomNameBox').y;
            roomNameNode.width = this.node.getChildByName('roomNameBox').width/2;
            roomNameNode.height = this.node.getChildByName('roomNameBox').height/2;
            roomNameNode.scale = 0.5;
            this.node.addChild(roomNameNode);
            this.node.getChildByName('roomNameBox').active=false;
            this.node.getChildByName('roomIDTxt').getComponent(cc.Label).string="房间号："+this.roomID;

            //密码
            let roomSecNode = new cc.Node('roomSecNode');
            let roomSecLabel = roomSecNode.addComponent(cc.Label);
            roomSecLabel.string=globalObj.roomSec;
            roomSecNode.x = this.node.getChildByName('secBox').x;
            roomSecNode.y = this.node.getChildByName('secBox').y;
            roomSecNode.width = this.node.getChildByName('secBox').width;
            roomSecNode.height = this.node.getChildByName('secBox').height;
            roomSecNode.scale = 0.5;
            this.node.addChild(roomSecNode);
            this.node.getChildByName('secBox').active=false;

            //规则
            let settingList = [1,5,6];
            for(let i=0;i<settingList.length;i++){
                let index = settingList[i];
                let settingNode = new cc.Node('settingNode');
                let settingLabel = settingNode.addComponent(cc.Label);
                if(globalObj.roomSettingList[index-1]==1){
                    settingLabel.string="●";
                }else{
                    settingLabel.string="○";
                }
                settingNode.x = this.node.getChildByName('setting'+index).x;
                settingNode.y = this.node.getChildByName('setting'+index).y;
                settingNode.width = this.node.getChildByName('setting'+index).width;
                settingNode.height = this.node.getChildByName('setting'+index).height;
                settingNode.scale = 0.5;
                this.node.addChild(settingNode);
                this.node.getChildByName('setting'+index).active=false;
            };
            this.node.getChildByName('confirmBtn').active=false;
        }else{
            //房主
            this.node.getChildByName('roomNameBox').getComponent(cc.EditBox).string = globalObj.roomName;
            this.node.getChildByName('roomIDTxt').getComponent(cc.Label).string="房间号："+this.roomID;
            this.node.getChildByName('secBox').getComponent(cc.EditBox).string = globalObj.roomSec;
            this.node.getChildByName('setting1').getComponent(cc.Toggle).isChecked = globalObj.roomSettingList[0]==1;
            this.node.getChildByName('setting5').getComponent(cc.Toggle).isChecked = globalObj.roomSettingList[4]==1;
            this.node.getChildByName('setting6').getComponent(cc.Toggle).isChecked = globalObj.roomSettingList[5]==1;

            this.node.getChildByName('confirmBtn').on(cc.Node.EventType.TOUCH_END,(event)=>{
                let roomName = this.node.getChildByName('roomNameBox').getComponent(cc.EditBox).string;
                let sec = this.node.getChildByName('secBox').getComponent(cc.EditBox).string;
                let isChecked1 = this.node.getChildByName('setting1').getComponent(cc.Toggle).isChecked;
                let isChecked5 = this.node.getChildByName('setting5').getComponent(cc.Toggle).isChecked;
                let isChecked6 = this.node.getChildByName('setting6').getComponent(cc.Toggle).isChecked;
                let settingList = [isChecked1?1:0,0,0,0,isChecked5?1:0,isChecked6?1:0,0];
                globalObj.send([10125, roomName, sec, settingList]);
                this.node.destroy();
            });
        };
    },

});
