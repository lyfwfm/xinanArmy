
import * as globalObj from './proto_lib';

var secIns=null;

cc.Class({
    extends: cc.Component,

    properties: {
        enterBtn:cc.Node,
        roomID:0,
        isSec:0,

        settingPrefab:cc.Prefab,
        blockPrefab:cc.Prefab,
        secPrefab:cc.Prefab,

        settingIns:{
            default:null,
            type:cc.Node
        },

        roomSettingList:[],
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

        this.enterBtn.on(cc.Node.EventType.TOUCH_END,(event)=>{
            if(this.isSec==0){
                globalObj.send([10105, this.roomID, ""]);
            }else{
                //密码控件，获取密码
                this.addBlockIns();
                
                setTimeout(()=>{
                    secIns = cc.instantiate(this.secPrefab);
                    secIns.getChildByName('yes_btn').on(cc.Node.EventType.TOUCH_END,(event)=>{
                        globalObj.send([10105, this.roomID, secIns.getChildByName('secEdit').getComponent(cc.EditBox).string]);
                    });
                    this.node.parent.addChild(secIns)
                },500);
                // globalObj.addSideByChild(this.node,secIns,this.enterBtn);
            }
        });

        //查看房间设置
        this.node.getChildByName('settingImg').on(cc.Node.EventType.TOUCH_END,(event)=>{
            //加入一层透明隔绝层
            this.addBlockIns();

            setTimeout(()=>{
                this.settingIns = cc.instantiate(this.settingPrefab);
                this.settingIns.getComponent('settingIns').insType = 2;
                this.settingIns.getComponent('settingIns').roomID=this.roomID;
                this.settingIns.getComponent('settingIns').roomName=this.node.getChildByName('roomNameTxt').getComponent(cc.Label).string;
                this.settingIns.getComponent('settingIns').roomSettingList=this.roomSettingList;
                this.settingIns.width = this.node.width;
                this.settingIns.height = this.node.height;
                this.node.parent.addChild(this.settingIns);
            },500);
        });
        //下面是用长按弹窗的方法实现，可以用  但是体验不好
        // this.node.getChildByName('settingImg').on(cc.Node.EventType.TOUCH_START,(event)=>{
        //     globalObj.onTouchStart(1.3,()=>{
        //         this.settingIns = cc.instantiate(this.settingPrefab);
        //         this.settingIns.getComponent('settingIns').insType = 2;
        //         this.settingIns.getComponent('settingIns').roomID=this.roomID;
        //         this.settingIns.getComponent('settingIns').roomName=this.node.getChildByName('roomNameTxt').getComponent(cc.Label).string;
        //         this.settingIns.getComponent('settingIns').roomSettingList=this.roomSettingList;
        //         this.settingIns.width = this.node.width;
        //         this.settingIns.height = this.node.height;
        //         this.node.parent.addChild(this.settingIns);
        //     });
        // });
        // this.node.getChildByName('settingImg').on(cc.Node.EventType.TOUCH_CANCEL,(event)=>{
        //     globalObj.onTouchCancel();
        // });
        // this.node.getChildByName('settingImg').on(cc.Node.EventType.TOUCH_END,(event)=>{
        //     globalObj.onTouchEnd();
        //     if(this.settingIns){
        //         this.settingIns.destroy();
        //         this.settingIns = null;
        //     };
        // });

    },

    // update (dt) {},

    //加入一层透明隔绝层
    addBlockIns(){
        let blockIns = cc.instantiate(this.blockPrefab);
        blockIns.width = this.node.parent.getChildByName('roomBackground').width;
        blockIns.height = this.node.parent.getChildByName('roomBackground').height;
        blockIns.x=0;blockIns.y=0;
        blockIns.on(cc.Node.EventType.TOUCH_END,(event)=>{
            if(this.settingIns){
                this.settingIns.destroy();
                this.settingIns = null;
            };
            if(secIns){
                secIns.destroy();
                secIns.null;
            }
        });        
        this.node.parent.addChild(blockIns);
    },
});
